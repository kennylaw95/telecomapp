-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:5:5
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
uses-sdk
ADDED from AndroidManifest.xml:7:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.android.support:appcompat-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:9:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:8:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.SEND_SMS
ADDED from AndroidManifest.xml:17:5
	android:name
		ADDED from AndroidManifest.xml:17:22
android:uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:19:5
	android:maxSdkVersion
		ADDED from AndroidManifest.xml:21:9
	android:name
		ADDED from AndroidManifest.xml:20:9
android:uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:22:5
	android:name
		ADDED from AndroidManifest.xml:22:30
android:uses-permission#android.permission.READ_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:23:5
	android:maxSdkVersion
		ADDED from AndroidManifest.xml:25:9
	android:name
		ADDED from AndroidManifest.xml:24:9
application
ADDED from AndroidManifest.xml:27:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.android.support:appcompat-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
	android:label
		ADDED from AndroidManifest.xml:30:9
	android:icon
		ADDED from AndroidManifest.xml:29:9
	android:name
		ADDED from AndroidManifest.xml:28:9
activity#com.TeamGO.CSE110.MainActivity
ADDED from AndroidManifest.xml:31:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:34:13
	android:label
		ADDED from AndroidManifest.xml:33:13
	android:theme
		ADDED from AndroidManifest.xml:35:13
	android:name
		ADDED from AndroidManifest.xml:32:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:36:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:37:17
	android:name
		ADDED from AndroidManifest.xml:37:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:39:17
	android:name
		ADDED from AndroidManifest.xml:39:27
activity#com.TeamGO.CSE110.LoginSignupActivity
ADDED from AndroidManifest.xml:42:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:44:13
	android:name
		ADDED from AndroidManifest.xml:43:13
activity#com.TeamGO.CSE110.Welcome
ADDED from AndroidManifest.xml:46:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:48:13
	android:name
		ADDED from AndroidManifest.xml:47:13
activity#com.TeamGO.CSE110.SignUpOptionActivity
ADDED from AndroidManifest.xml:50:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:54:13
	android:label
		ADDED from AndroidManifest.xml:52:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:53:13
	android:name
		ADDED from AndroidManifest.xml:51:13
meta-data#android.support.PARENT_ACTIVITY
ADDED from AndroidManifest.xml:55:13
	android:value
		ADDED from AndroidManifest.xml:57:17
	android:name
		ADDED from AndroidManifest.xml:56:17
activity#com.TeamGO.CSE110.RetailSignUpActivity
ADDED from AndroidManifest.xml:59:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:63:13
	android:label
		ADDED from AndroidManifest.xml:61:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:62:13
	android:name
		ADDED from AndroidManifest.xml:60:13
activity#com.TeamGO.CSE110.CommercialSignUpActivity
ADDED from AndroidManifest.xml:68:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:72:13
	android:label
		ADDED from AndroidManifest.xml:70:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:71:13
	android:name
		ADDED from AndroidManifest.xml:69:13
activity#com.TeamGO.CSE110.MyServicesListActivity
ADDED from AndroidManifest.xml:77:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:81:13
	android:label
		ADDED from AndroidManifest.xml:79:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:80:13
	android:name
		ADDED from AndroidManifest.xml:78:13
activity#com.TeamGO.CSE110.AddServicesListActivity
ADDED from AndroidManifest.xml:86:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:90:13
	android:label
		ADDED from AndroidManifest.xml:88:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:89:13
	android:name
		ADDED from AndroidManifest.xml:87:13
activity#com.TeamGO.CSE110.AdminController
ADDED from AndroidManifest.xml:95:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:99:13
	android:label
		ADDED from AndroidManifest.xml:97:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:98:13
	android:name
		ADDED from AndroidManifest.xml:96:13
activity#com.TeamGO.CSE110.MyBillActivity
ADDED from AndroidManifest.xml:104:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:108:13
	android:label
		ADDED from AndroidManifest.xml:106:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:107:13
	android:name
		ADDED from AndroidManifest.xml:105:13
activity#com.TeamGO.CSE110.CreatePackageActivity
ADDED from AndroidManifest.xml:113:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:117:13
	android:label
		ADDED from AndroidManifest.xml:115:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:116:13
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:118:13
	android:name
		ADDED from AndroidManifest.xml:114:13
activity#com.TeamGO.CSE110.MarketingAddService
ADDED from AndroidManifest.xml:123:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:127:13
	android:label
		ADDED from AndroidManifest.xml:125:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:126:13
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:128:13
	android:name
		ADDED from AndroidManifest.xml:124:13
activity#com.TeamGO.CSE110.MarketRepActivity
ADDED from AndroidManifest.xml:133:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:137:13
	android:label
		ADDED from AndroidManifest.xml:135:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:136:13
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:138:13
	android:name
		ADDED from AndroidManifest.xml:134:13
activity#com.TeamGO.CSE110.EditPackages
ADDED from AndroidManifest.xml:143:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:147:13
	android:label
		ADDED from AndroidManifest.xml:145:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:146:13
	android:name
		ADDED from AndroidManifest.xml:144:13
activity#com.TeamGO.CSE110.MarketingRepRemoveServices
ADDED from AndroidManifest.xml:149:9
	android:label
		ADDED from AndroidManifest.xml:151:13
	android:name
		ADDED from AndroidManifest.xml:150:13
