package com.TeamGO.CSE110;

import com.parse.*;

/**
 * Created by Jeffery on 2/8/15.
 */
@ParseClassName("Internet")
public class InternetService extends ParseObject implements Service {

    // service members
    String name;
    String user; //owner of this service
    double price;
    int subscriptionPeriod; //months

    public InternetService() {
        name = "Internet";
        price = 75.00;
        user = null; //to be initialized when we add internet service to customer account
        // default
        subscriptionPeriod = 12;

    }

    @Override
    public void setAvailability(boolean availability) {
        put("Available", availability);
    }

    @Override
    public boolean getAvailability() {

        return getBoolean("Available");
    }
    public double getPrice() {
        return getDouble("Price");
    }

    public void setPrice(double price) {
        put("Price", price);
    }

    public String getName(){
        return getString("Name");
    }
    public void setName(String name) {
        put("Name", name);
    }
    public int getSubscriptionPeriod() {return getInt("Days");}
    public void setSubscriptionPeriod(int period) {put("Days", period);}

    @Override
    public String toString(){
        return getName();
    }
}
