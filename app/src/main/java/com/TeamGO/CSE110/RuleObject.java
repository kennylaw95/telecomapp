package com.TeamGO.CSE110;

import java.util.ArrayList;

/**
 * Created by Jessica on 3/5/2015.
 */
public class RuleObject implements Subject{
    private Assessor assessor;
    private Action action;
    private Result result;
    private ArrayList<Observer> observers;

    public RuleObject(Assessor assessor, Action action) {
        this.assessor = assessor;
        this.action = action;
        this.result = new Result();
    }


    public boolean checkRule(Properties prop) {
        if(assessor.evaluate(prop)) {
            action.execute(prop);
            result.setActionResults(action.getErrors());
            result.setAssessorResults(assessor.getErrors());
            return true;
        }
        result.setAssessorResults(assessor.getErrors());
        return false;
    }


    public Result getResults() {
        return result;
    }


    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void addObserver(Observer observer) {

    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);

    }

    @Override
    public void notifyObserver() {

    }

    public double getBill(){
        return 0;
    }

    public double getThreshold(){
        return 0;
    }


    public void notifyObservers(Observer o) {
        for(Observer observer :observers){
            observer.update();
        }

    }
}

