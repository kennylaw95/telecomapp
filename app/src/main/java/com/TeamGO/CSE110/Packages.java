package com.TeamGO.CSE110;
import com.parse.*;

/**
 * Created by Jessica on 2/22/2015.
 */
@ParseClassName("Packages")
public class Packages extends ParseObject {

    public Packages(){

    }

    public void setAvailability(boolean availability) {
        put("Available", availability);
    }
    public boolean getAvailability() {

        return getBoolean("Available");
    }
    //package name
    public void setName(String name){ put("Name", name);}
    public String getName(){return getString("Name");}


    public void setCost( double cost){
        put("Cost", cost);
    }
    public Double getCost(){
        return getDouble("Cost");
    }

    public int getDuration() {return getInt("Duration");}
    public void setDuration(int duration) {put("Duration", duration);}

    //adds item to services array
    public void addService(ParseObject service){
        //query for member name

        addUnique("Services", service);

    }

    @Override
    public String toString(){
        return getName();
    }
}
