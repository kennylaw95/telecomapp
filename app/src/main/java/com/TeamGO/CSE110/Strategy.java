package com.TeamGO.CSE110;

import com.parse.ParseUser;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jessica on 3/6/2015.
 */
public class Strategy {

    List<RuleObject> Rules;

    public Strategy() {
        Rules = new LinkedList<RuleObject>();
        // customer bill cannot exceed threshold
        Rules.add(new RuleObject(new Assessor(),
                new Action()));

    }


    public void applyRule(ParseUser customer) {
        Properties prop = new Properties(customer);
        for (RuleObject rule : Rules) {
            rule.checkRule(prop);
            //Report possible errors
            if(rule.getResults().hasErrors()) {
                if (rule.getResults().getAssessorResults()!=null)
                    System.err.println(rule.getResults().getAssessorResults());
                if (rule.getResults().getActionResults()!=null)
                    System.err.println(rule.getResults().getActionResults());
            }
        }
    }
}

