package com.TeamGO.CSE110;

/**
 * Created by Jeffery on 2/8/15.
 */

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/*
    Notice: Changed the extension. FragmentActivity is a subclass of activity tho,
    thus not much code change. FA is used for getting the authority over dialog positive(confirm)
    and negative(cancel) events.Implements the long shit to use the dialog. Check Android tutorial
    and documents on Dialog for more info.
 */
public class AdminController extends FragmentActivity
        implements DeleteServiceConfirmDialogFragment.DeleteConfirmDialogListener,
        AddServiceConfirmDialogFragment.AddConfirmDialogListener{
    //Current Parse User of whom we act on behalf.
    ParseUser currentUser;
    ParseObject srvcToDelete, srvcToAdd;
    Services customerServices, subServices, availServices;

    EditText editTextUser;
    Button searchUser, logout;

    String selectedSubServiceName, selectedAvailServiceName, username;
    ListView subServiceListView, availServiceListView;
    ArrayAdapter<ParseObject> subAdapter, availAdapter;
    Button deleteServiceButton,addServiceButton;
    ArrayList<ParseObject> subSrvcPkgAL,availSrvcPkgAL;

    /*
        onCreate: Init the UI, set up button onClickListeners
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from layout xml
        setContentView(R.layout.admin_controller);

        customerServices = null;

        //Get current logged-in user
        currentUser = ParseUser.getCurrentUser();
        username = currentUser.getUsername();

        //The search bar and search button
        editTextUser = (EditText) findViewById(R.id.editTextUsers);
        searchUser = (Button)findViewById(R.id.searchUserButton);

        availSrvcPkgAL = new ArrayList<ParseObject>();//Here one should specify the type of object
        //getting stored. In actual implementation it would
        //be either service or package
        availServiceListView = (ListView) findViewById(R.id.availsrvclistview);//Get the listview

        /*Get the list of all services and packages*/
        ParseQuery<InternetService> internetQuery = new ParseQuery<InternetService>("Internet");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<InternetService> objs = internetQuery.find();
            for (InternetService o : objs) {
                if (!availSrvcPkgAL.contains(o) && o.getAvailability())
                    availSrvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<WirelessService> wirelessQuery = new ParseQuery<WirelessService>("Wireless");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelessService> objs = wirelessQuery.find();
            for (WirelessService o : objs) {
                if (!availSrvcPkgAL.contains(o) && o.getAvailability())
                    availSrvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<WirelineService> wirelineQuery = new ParseQuery<WirelineService>("Wireline");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelineService> objs = wirelineQuery.find();
            for (WirelineService o : objs) {
                if (!availSrvcPkgAL.contains(o) && o.getAvailability())
                    availSrvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<Packages> packageQuery = new ParseQuery<Packages>("Packages");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<Packages> objs = packageQuery.find();
            for (Packages o : objs) {
                if (!availSrvcPkgAL.contains(o))
                    availSrvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        availAdapter = new ArrayAdapter<ParseObject>(this,
                android.R.layout.simple_list_item_1, availSrvcPkgAL);
        //Setup the adapter between the listview and the array.
        //First arg should be the activity, second leave it as is, third one the ArrayList
        //storing the object you would like to display.
        availServiceListView.setAdapter(availAdapter);
        //Attach the adapter to the listview

        availServiceListView.setOnItemClickListener( //Set up click listener. It is trigger if a item in the list
                //is clicked.
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView parent, View v, int position, long id) {
                        selectedAvailServiceName = (String)((TextView)v).getText();
                        //All item in the listview is a TextView created by calling the toString() of
                        //each object in the forementioned ArrayList.I had overrided the toString of
                        //different services classes
                    }
                });

        //Set up the button
        addServiceButton = (Button) findViewById(R.id.adminaddservicebutton);
        addServiceButton.setOnClickListener(new OnClickListener() {
            //In this test I make the button act like a deleter, deleting the item selected.
            @Override
            public void onClick(View v) {
                if (customerServices == null){
                    Toast.makeText(
                            getApplicationContext(),
                            "You aren't acting on anyone's behave",
                            Toast.LENGTH_LONG).show();
                    return;
                }
                if (selectedAvailServiceName == null){
                    Toast.makeText(
                            getApplicationContext(),
                            "You haven't selected anything",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                ArrayList<ParseObject> srcList = (ArrayList<ParseObject>) customerServices.get("ServicesList");
                for (ParseObject o: srcList){
                    String srvcName = null;
                    try{
                        srvcName = o.fetchIfNeeded().getString("Name");
                    }catch (ParseException e){}

                    if (srvcName.equals(selectedAvailServiceName)){
                        Toast.makeText(
                                getApplicationContext(),
                                "You can't resubscribe " + selectedAvailServiceName,
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                ArrayList<ParseObject> pkgList = (ArrayList<ParseObject>) customerServices.get("PackagesList");
                for (ParseObject o: pkgList){
                    String srvcName = null;
                    try{
                        srvcName = o.fetchIfNeeded().getString("Name");
                    }catch (ParseException e){}

                    if (srvcName.equals(selectedAvailServiceName)){
                        Toast.makeText(
                                getApplicationContext(),
                                "You can't resubscribe " + selectedAvailServiceName,
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                srvcToAdd = null;
                for(ParseObject o: availSrvcPkgAL){
                    if (o.getString("Name").equals(selectedAvailServiceName))
                        srvcToAdd = o;
                }
                if (srvcToAdd != null) {
                    showNoticeDialogAdd();
                }
                //onResume();
            }
        });





        //Define the button action onclick.
        searchUser.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                //Get search string.
                final String theUser = editTextUser.getText().toString();
                //Setup query to get the userlist in DB
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.findInBackground(new FindCallback<ParseUser>() {
                    public void done(List<ParseUser> users, ParseException e) {
                        if (e == null) { //No exception caught
                            for(ParseUser u : users) //Traverse the user list to find the target user
                            {
                                if(u.getUsername().equals(theUser)) //If found
                                {
                                    currentUser = u; //Set current user
                                    //Toast a notice
                                    Toast.makeText(getApplicationContext(),
                                            ""+u.getUsername()+" is found. Acting on behalf",
                                            Toast.LENGTH_LONG).show();
                                    // QUERY THE SERVICES OBJECTS
                                    ParseQuery<Services> query = new ParseQuery<Services>("ServicesStatus");
                                    query.findInBackground(new FindCallback<Services>() {
                                        public void done(List<Services> objs, ParseException e) {
                                            if (e == null) {
                                                for (Services o : objs) {
                                                    //Find the pairing serviceStatus entry.
                                                    if (o.get("Username").equals(theUser)) {
                                                        subServices = o;
                                                        customerServices =
                                                                ParseObject.createWithoutData(Services.class,
                                                                        subServices.getObjectId());
                                                        onResume(); //Refresh UI.
                                                    }
                                                }
                                            } else {}
                                        }
                                    });
                                }

                            }
                            //The target user is not found in the list.
                            if (!currentUser.getUsername().equals(theUser)){
                                Toast.makeText(getApplicationContext(),
                                        ""+theUser+" not found",
                                        Toast.LENGTH_LONG).show();
                            }
                        } else { //Something is wrong with the connection
                            Toast.makeText(getApplicationContext(),
                                    "Something went wrong",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        });

        subSrvcPkgAL = new ArrayList<ParseObject>();
        subAdapter = new ArrayAdapter<ParseObject>(this,
                android.R.layout.simple_list_item_1, subSrvcPkgAL);
        //Setup the adapter between the listview and the array.
        subServiceListView = (ListView) findViewById(R.id.subsrvclistview);
        subServiceListView.setAdapter(subAdapter);
        //Attach the adapter to the listview
        subServiceListView.setOnItemClickListener( //Set up click listener. It is trigger if a item in the list
                //is clicked.
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView parent, View v, int position, long id) {
                        selectedSubServiceName = (String)((TextView)v).getText();
                    }
                });
        deleteServiceButton = (Button) findViewById(R.id.admindeleteservicebutton);
        deleteServiceButton.setOnClickListener(new OnClickListener() {
            //In this test I make the button act like a deleter, deleting the item selected.
            @Override
            public void onClick(View v) {
                if (customerServices == null){
                    Toast.makeText(
                            getApplicationContext(),
                            "You aren't acting on anyone's behave",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                if (selectedSubServiceName == null){
                    Toast.makeText(
                            getApplicationContext(),
                            "You haven't selected anything",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                srvcToDelete = null;
                for(ParseObject o: subSrvcPkgAL){
                    if (o.getString("Name").equals(selectedSubServiceName))
                        srvcToDelete = o;
                }

                if (srvcToDelete != null) {
                    showNoticeDialog();
                }else{
                    Toast.makeText(
                            getApplicationContext(),
                            "You can't re-unsubscribe " + selectedSubServiceName,
                            Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });


        logout = (Button) findViewById(R.id.logout);

        // Logout Button Click Listener
        logout.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // Logout current user
                ParseUser.logOut();
                finish();
            }
        });
    }

    /*
        onResume: Check Android document on Activity for more detailed discuss of different states
        of activity. Long story short, whenever the app reaches the page either from other page or
        from the dialog, this method would be called.
     */
    public void onResume(){
        super.onResume();
        if (customerServices == null){
            return;
        }

        subSrvcPkgAL.clear();
        try {
            customerServices.fetchIfNeeded();
        }catch (ParseException e){}
        subSrvcPkgAL.addAll((ArrayList<ParseObject>) customerServices.get("ServicesList"));
        subSrvcPkgAL.addAll((ArrayList<ParseObject>) customerServices.get("PackagesList"));
        for (ParseObject o: subSrvcPkgAL){
            try {
                o.fetchIfNeeded();
            }catch (ParseException e){}
        }
        subAdapter.notifyDataSetChanged();
    }

    //Call this method to pop the confirmation dialog for deleting services.
    public void showNoticeDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new DeleteServiceConfirmDialogFragment();
        dialog.show(getSupportFragmentManager(), "DeleteServiceConfirmDialogFragment");
    }

    //Call this method to pop the confirmation dialog for adding services.
    public void showNoticeDialogAdd() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new AddServiceConfirmDialogFragment();
        dialog.show(getSupportFragmentManager(), "AddServiceConfirmDialogFragment");
    }

    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    // defined by the NoticeDialogFragment.NoticeDialogListener interface
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        customerServices.removeService(srvcToDelete);
        customerServices.saveInBackground();
        Toast.makeText(
                getApplicationContext(),
                "You unsubscribed " + selectedSubServiceName,
                Toast.LENGTH_LONG).show();

        onResume();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
        // Nothing happens, sun still rises
        onResume();
    }

    @Override
    public void onDialogPositiveClickAdd(DialogFragment dialog) {
        customerServices.addService(srvcToAdd);
        Toast.makeText(
                getApplicationContext(),
                ""+ selectedAvailServiceName + " is subscribed",
                Toast.LENGTH_LONG).show();
        try{
            customerServices.save();
        }catch (ParseException e){}
        onResume();
    }

    @Override
    public void onDialogNegativeClickAdd(DialogFragment dialog) {
        // User touched the dialog's negative button
        // Nothing happens, sun still rises
        onResume();
    }
}