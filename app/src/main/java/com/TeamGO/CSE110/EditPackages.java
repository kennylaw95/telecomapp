package com.TeamGO.CSE110;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SignUpCallback;

import java.util.ArrayList;
import java.util.List;


public class EditPackages extends Activity {

    Button editPkgBtn;
    Button commitChangesBtn;
    ParseObject editPackage;
    Packages packageToEdit;

    ListView packagesListView;
    //ListView currentServicesView;
    ListView serviceListView;
    //ArrayAdapter<ParseObject> currentServicesAdapter;

    //Context context = this;

    //Empty textbox forms
    TextView packageName;
    EditText duration;
    EditText price;

    //Textbox labels
    String packageNametxt;
    int durationtxt;
    double pricetxt;


   // Packages newPackage = ParseObject.create(Packages.class);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_packages);


        //show the package list
        packagesListView = (ListView) findViewById(R.id.packageList);
        serviceListView = (ListView) findViewById(R.id.currentPackagesList);



        //define array values to show in listview

        ArrayList<ParseObject> packages = new ArrayList<ParseObject>();
        ArrayList<ParseObject> services = new ArrayList<>();

        //add internet services to services list
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Packages");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<ParseObject> objs = query.find();
            for (ParseObject o : objs) {
                if (o.getBoolean("Available"))
                packages.add(o);
            }
        }catch (ParseException e){}



        //define new adapter
        ArrayAdapter<ParseObject> packagesAdapter = new ArrayAdapter<ParseObject>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, packages);

        //assign adapter to list view
        packagesListView.setAdapter(packagesAdapter);

        //ListView item click Listener
        packagesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            editPackage = (ParseObject) packagesListView.getItemAtPosition(position);
            packageToEdit = ParseObject.createWithoutData(Packages.class, editPackage.getObjectId());



            }
        });

        ArrayList<ParseObject> serviceList = new ArrayList<ParseObject>();

        //add internet services to services list
        ParseQuery<ParseObject> internetQuery = new ParseQuery<ParseObject>("Internet");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<ParseObject> objs = internetQuery.find();
            for (ParseObject o : objs) {
                if(o.getBoolean("Available")){serviceList.add(o);}
            }
        }catch (ParseException e){}

        //add wireless services to services list
        ParseQuery<ParseObject> wirelessQuery = new ParseQuery<ParseObject>("Wireless");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<ParseObject> objs = wirelessQuery.find();
            for (ParseObject o : objs) {
                if(o.getBoolean("Available")){serviceList.add(o);}
            }
        }catch (ParseException e){}

        //add wireline services to services list
        ParseQuery<ParseObject> wirelineQuery = new ParseQuery<ParseObject>("Wireline");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<ParseObject> objs = wirelineQuery.find();
            for (ParseObject o : objs) {
                if(o.getBoolean("Available")){serviceList.add(o);}
            }
        }catch (ParseException e){}

        //define new adapter
        ArrayAdapter<ParseObject>Adapter = new ArrayAdapter<ParseObject>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, serviceList);

        //assign adapter to list view
        serviceListView.setAdapter(Adapter);

        //ListView item click Listener
        serviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //listview clicked item index
              /*  int itemPosition = position;
                //ListView Clicked item value
                String itemValue = (String)internetListView.getItemAtPosition(position);

                //add service object to package
            */
                if (packageToEdit == null) {
                    Toast.makeText(
                            getApplicationContext(),
                            "No package selected",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                serviceListView.setSelected(true);
                packageToEdit.addService((ParseObject)serviceListView.getItemAtPosition(position));
                Toast.makeText(
                        getApplicationContext(),
                        "Clicked",
                        Toast.LENGTH_LONG).show();

                //newPackage.saveInBackground();

            }
        });





        packageName = (EditText) findViewById(R.id.packageName);
        price = (EditText) findViewById(R.id.price);
        duration = (EditText)findViewById(R.id.duration);

        editPkgBtn = (Button) findViewById(R.id.editPackageBtn);
        commitChangesBtn = (Button) findViewById(R.id.commitChangesBtn);

        editPkgBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

            if (packageToEdit == null) {
                Toast.makeText(
                        getApplicationContext(),
                        "No package selected",
                        Toast.LENGTH_LONG).show();
                return;
            }

            try{
                price.setText(String.valueOf(packageToEdit.fetchIfNeeded().getDouble("Cost")));
            }catch(ParseException e){
                e.printStackTrace();
            }
            try{
                duration.setText(String.valueOf(packageToEdit.fetchIfNeeded().getInt("Duration")));
            }catch(ParseException pe){
                pe.printStackTrace();
            }
            try{
                packageName.setText(packageToEdit.fetchIfNeeded().getString("Name"));
            }catch(ParseException p){
                p.printStackTrace();
            }


            }
        });

        commitChangesBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                if (packageToEdit == null) {
                    Toast.makeText(
                            getApplicationContext(),
                            "No package selected",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                // Retrieve the text entered from the EditText
                packageNametxt = packageName.getText().toString();
                pricetxt = Double.parseDouble(price.getText().toString());
                durationtxt = Integer.parseInt(duration.getText().toString());

                packageToEdit.setCost(pricetxt);
                packageToEdit.setDuration(durationtxt);
                packageToEdit.setName(packageNametxt);
                packageToEdit.saveInBackground();




            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_package, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*private void showList(){
        try {
            ArrayList<ParseObject> currentServices = (ArrayList<ParseObject>) packageToEdit.fetchIfNeeded().get("ServicesList");
            //define new adapter
            currentServicesAdapter = new ArrayAdapter<ParseObject>(
                    this, android.R.layout.simple_list_item_1, android.R.id.text1, currentServices);

            //assign adapter to list view
            currentServicesView.setAdapter(currentServicesAdapter);
            currentServicesView.setVisibility(View.VISIBLE);

            //ListView item click Listener
            currentServicesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //listview clicked item index


                }
            });
        } catch (ParseException pex) {
            pex.printStackTrace();
        }
    }*/

}
