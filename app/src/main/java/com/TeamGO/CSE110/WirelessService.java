package com.TeamGO.CSE110;
import com.parse.*;
import java.util.*;

/**
 * Created by Jeffery on 2/8/15.
 */
@ParseClassName("Wireless")
public class WirelessService extends ParseObject implements Service {

        // service members
    String name;
    String user; //owner of the service
    double price;
    boolean available;

    int subscriptionPeriod; //months

    public WirelessService() {
        name = "Wireless";
        price = 60.00;
        user = null; //to be initialized upon addition of a package
        // default
        subscriptionPeriod = 12;

    }


    @Override
    public void setAvailability(boolean availability) {
        put("Available", availability);
    }

    @Override
    public boolean getAvailability() {

        return getBoolean("Available");
    }

    public double getPrice() {
        return getDouble("Price");
    }

    public void setPrice(double price) {
        put("Price", price);
    }

    public String getName(){
        return getString("Name");
    }
    public void setName(String name) {
        put("Name", name);
    }
    public int getSubscriptionPeriod() {return getInt("Days");}
    public void setSubscriptionPeriod(int period) {put("Days", period);}

    @Override
    public String toString(){
        return getName();
    }
}
