package com.TeamGO.CSE110;

/**
 * Created by flyfire2002 on 02/09/2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class AddServiceConfirmDialogFragment extends DialogFragment {
    //Declare a interface to allow the MyServicesListActivity to behave according to users'
    //choices on the dialog.
    public interface AddConfirmDialogListener {
        public void onDialogPositiveClickAdd(DialogFragment dialog);
        public void onDialogNegativeClickAdd(DialogFragment dialog);
    }

    AddConfirmDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    // activity is the host (MyServicesListActivity)
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (AddConfirmDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement AddConfirmDialogListener");
        }
    }

    //Create the dialog when necessary, setup texts and buttons
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.AddConfirmMsg)
                .setPositiveButton(R.string.Add, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Delegate to the host to react.
                        mListener.onDialogPositiveClickAdd(AddServiceConfirmDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Same here
                        mListener.onDialogNegativeClickAdd(AddServiceConfirmDialogFragment.this);
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
