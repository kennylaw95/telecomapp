package com.TeamGO.CSE110;

import android.telephony.SmsManager;

import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Jessica on 3/7/2015.
 */
@ParseClassName("_User")
public class CommercialCustomer extends ParseUser implements Observer, Customer{
    private String address;
    private String name;

    private Strategy thresholdStrategy;
    private ArrayList<ParseObject> Services = new ArrayList<ParseObject>();
    Services subject;
    //private ArrayList<String> Services = new ArrayList<String>();

    public CommercialCustomer(){
        address = null;
        name = null;

        thresholdStrategy = new Strategy();

    }

    @Override
    public String getNumber() {
        return getString("PhoneNumber");
    }

    @Override
    public void setNumber(String number) {
        put("PhoneNumber", number);

    }

    public Strategy getThresholdStrategy(){
        return this.thresholdStrategy;
    }
    public void addService(ParseObject service){

        this.addUnique("services", service);

    }

    public void setServices(ArrayList<ParseObject> services){
        put("services", Arrays.asList(services));
    }



    public void setAddress(String address) {
        put("address", address);
    }

    public String getAddress(){
        return getString("address");
    }

    public void setName(String name) {
        put("name", name);
    }

    public String getName(){
        return getString("name");
    }

    //unique customer service methods

    public void setCompany(String company){
        put("Company", company);
    }
    public void setResponsibleParty(String person){
        put("ResponsibleParty", person);
    }


    @Override
    public void addSubject(Services subject){
        this.subject = subject;
        put("Subject", subject);
    }
    @Override
    public void update() {
        try {
            fetchIfNeeded();
            subject = (Services)getParseObject("Subject");
            subject.fetchIfNeeded();
        }catch (ParseException e){}
        SmsManager smsManager = SmsManager.getDefault();
        if(subject.getBill() >subject.getThreshold()) {
            smsManager.sendTextMessage(getNumber(), null, "Your balance " + subject.getBill() +
                    " exceeds your set threshold " + subject.getThreshold() + ".", null, null);
        }
    }
}
