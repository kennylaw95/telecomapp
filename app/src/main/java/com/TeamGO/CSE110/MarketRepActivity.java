package com.TeamGO.CSE110;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.parse.ParseUser;

/**
 * Created by Law on 2/24/15.
 */
public class MarketRepActivity extends Activity {

    Button addToServices;
    Button existingPackages;
    Button createPackages;
    Button logout;
    Button deleteServices;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.market_rep_landing);

        addToServices = (Button) findViewById(R.id.addToServices);
        existingPackages = (Button) findViewById(R.id.addToExistPackages);
        createPackages = (Button) findViewById(R.id.createNewPackage);
        deleteServices = (Button) findViewById(R.id.deleteServices);

        addToServices.setOnClickListener(new View.OnClickListener() {
            //In this test I make the button act like a deleter, deleting the item selected.
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MarketRepActivity.this,
                        MarketingAddService.class);
                startActivity(intent);
            }
        });

        existingPackages.setOnClickListener(new View.OnClickListener() {
            //In this test I make the button act like a deleter, deleting the item selected.
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MarketRepActivity.this,
                        EditPackages.class);
                startActivity(intent);
            }
        });

        createPackages.setOnClickListener(new View.OnClickListener() {
            //In this test I make the button act like a deleter, deleting the item selected.
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MarketRepActivity.this,
                        CreatePackageActivity.class);
                startActivity(intent);
            }
        });
        deleteServices.setOnClickListener(new View.OnClickListener() {
            //In this test I make the button act like a deleter, deleting the item selected.
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MarketRepActivity.this,
                        MarketingRepRemoveServices.class);
                startActivity(intent);
            }
        });



        logout = (Button) findViewById(R.id.logout);

        // Logout Button Click Listener
        logout.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // Logout current user
                ParseUser.logOut();
                finish();
            }
        });
    }
}
