package com.TeamGO.CSE110;


import com.parse.*;
import android.telephony.SmsManager;
import java.util.*;

/**
 * Created by Jessica on 2/8/2015.
 */

@ParseClassName("_User")
public class RetailCustomer extends ParseUser implements Customer, Observer{
    private String address;
    private String name;
    private boolean hasWireless;
    private boolean hasInternet;
    private boolean hasWireline;
    private ArrayList<ParseObject> Services = new ArrayList<ParseObject>();
    Services subject;
    //private ArrayList<String> Services = new ArrayList<String>();

    public RetailCustomer(){
        address = null;
        name = null;
        hasWireless = false;
        hasWireline = false;
        hasInternet = false;

    }


    @Override
    public String getNumber() {
        return getString("PhoneNumber");
    }

    @Override
    public void setNumber(String number) {
        put("PhoneNumber", number);

    }

    @Override
    public Strategy getThresholdStrategy() {
        return new Strategy();
    }

    public void addService(ParseObject service){

        this.addUnique("services", service);

    }

    public void setServices(ArrayList<ParseObject> services){
        put("services", Arrays.asList(services));
    }


    public void setAddress(String address) {
        put("address", address);
    }

    public String getAddress(){
        return getString("address");
    }

    public void setName(String name) {
        put("name", name);
    }

    public String getName(){
        return getString("name");
    }


    public boolean getHasInternet() {
        return getBoolean("internet");
    }

    public void setHasInternet(boolean hasInternet) {
         put("internet", hasInternet);
    }

    public boolean getHasWireless() {
        return getBoolean("wireless");
    }

    public void setHasWireless(boolean hasWireless) {

        put("wireless", hasWireless);
    }

    public boolean getHasWireline() {
        return getBoolean("wireline");
    }

    public void setHasWireline(boolean hasWireline) {

        put("wireline",hasWireline);
    }

    @Override
    public void addSubject(Services subject){
        this.subject = subject;
        put("Subject", subject);
    }
    @Override
    public void update() {
        try {
            fetchIfNeeded();
            subject = (Services)getParseObject("Subject");
            subject.fetchIfNeeded();
        }catch (ParseException e){}
        SmsManager smsManager = SmsManager.getDefault();
        if(subject.getBill()>subject.getThreshold()) {
            smsManager.sendTextMessage(getNumber(), null, "Your balance " + subject.getBill() +
                    " exceeds your set threshold " + subject.getThreshold() + ".", null, null);
        }
    }
}
