package com.TeamGO.CSE110;

import com.parse.ParseUser;

/**
 * Created by Jessica on 3/5/2015.
 */
public class Properties {

    double billThreshold;
    ParseUser customer;

    public double getBillThreshold() {
        return billThreshold;
    }

    public double getCurrentBill() {
        return customer.getDouble("Bill");
    }
    public ParseUser getCustomer() {
        return customer;
    }

    public Properties(ParseUser telecomCustomer) {
        this.customer = telecomCustomer;
        this.billThreshold = telecomCustomer.getDouble("Bill Threshold");

    }

}
