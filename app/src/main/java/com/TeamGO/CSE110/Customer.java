package com.TeamGO.CSE110;

import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Jessica on 3/7/2015.
 */
public interface Customer{

    public String getNumber();
    public void setNumber(String number);
    public Strategy getThresholdStrategy();
    public void addService(ParseObject service);
    public void setServices(ArrayList<ParseObject> services);

    public void setAddress(String address);
    public String getAddress();

    public void setName(String name);

    public String getName();

}
