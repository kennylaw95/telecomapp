package com.TeamGO.CSE110;
 
import com.parse.Parse;
import com.parse.ParseACL;
 
import com.parse.ParseUser;
import com.parse.ParseObject;
 
import android.app.Application;
 
public class ParseApplication extends Application {
 
    @Override
    public void onCreate() {
        super.onCreate();

        ParseUser.registerSubclass(RetailCustomer.class);
        ParseUser.registerSubclass(InternetService.class);
        ParseUser.registerSubclass(WirelineService.class);
        ParseUser.registerSubclass(WirelessService.class);
        ParseUser.registerSubclass(Services.class);
        ParseUser.registerSubclass(Packages.class);
        ParseUser.registerSubclass(CommercialCustomer.class);

 
        // Parse key codes
        Parse.initialize(this, "AMTvK2ZetdSyAHBIiVnZaf4UVdOcLAfCTGsZdnke", "yFBpW6GF61xbO8N9PT3qTbIDLuESuyzSXMGx2LHP");
 
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
 
        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
 
        ParseACL.setDefaultACL(defaultACL, true);
    }
 
}