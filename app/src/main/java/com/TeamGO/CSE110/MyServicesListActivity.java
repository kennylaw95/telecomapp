package com.TeamGO.CSE110;

/**
 * Created by Jeffery on 2/8/15.
 */

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/*
    Notice: Changed the extension. FragmentActivity is a subclass of activity tho,
    thus not much code change. FA is used for getting the authority over dialog positive(confirm)
    and negative(cancel) events.Implements the long shit to use the dialog. Check Android tutorial
    and documents on Dialog for more info.
 */
public class MyServicesListActivity extends FragmentActivity
        implements DeleteServiceConfirmDialogFragment.DeleteConfirmDialogListener {
    ParseUser currentUser;
    ParseObject srvcToDelete;

    String username;
    Customer customer;
    Services customerServices, services;

    String selectedServiceName;
    ListView serviceListView;
    ArrayAdapter<ParseObject> adapter;
    Button deleteServiceButton;
    ArrayList<ParseObject> srvcPkgAL;

    /*
        onCreate: Init the UI, set up button onClickListeners
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from layout xml
        setContentView(R.layout.my_services_list);

        //Get current logged-in user
        currentUser = ParseUser.getCurrentUser();
        username = currentUser.getUsername();
        if (currentUser.getInt("Usertype") == 0) {
            customer = ParseObject.createWithoutData(RetailCustomer.class, currentUser.getObjectId());
        }else{
            customer = ParseObject.createWithoutData(CommercialCustomer.class, currentUser.getObjectId());
        }

        ParseQuery<Services> query = new ParseQuery<Services>("ServicesStatus");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<Services> objs = query.find();
            for (Services o : objs) {
                if (o.get("Username").equals(currentUser.getUsername())) {
                    services = o;
                }
            }
        }catch (ParseException e){}

        customerServices = ParseObject.createWithoutData(Services.class, services.getObjectId());

        srvcPkgAL = new ArrayList<ParseObject>();
        adapter = new ArrayAdapter<ParseObject>(this,
                android.R.layout.simple_list_item_1, srvcPkgAL);
        //Setup the adapter between the listview and the array.
        serviceListView = (ListView) findViewById(R.id.myservicelistview);
        serviceListView.setAdapter(adapter);
        //Attach the adapter to the listview
        serviceListView.setOnItemClickListener( //Set up click listener. It is trigger if a item in the list
                //is clicked.
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView parent, View v, int position, long id) {
                        selectedServiceName = (String)((TextView)v).getText();
                    }
                });
        deleteServiceButton = (Button) findViewById(R.id.deletebutton);
        deleteServiceButton.setOnClickListener(new OnClickListener() {
            //In this test I make the button act like a deleter, deleting the item selected.
            @Override
            public void onClick(View v) {
                if (selectedServiceName == null){
                    Toast.makeText(
                            getApplicationContext(),
                            "You haven't selected anything",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                ArrayList<ParseObject> srcList = (ArrayList<ParseObject>) customerServices.get("ServicesList");

                srvcToDelete = null;
                for(ParseObject o: srvcPkgAL){
                    if (o.getString("Name").equals(selectedServiceName))
                        srvcToDelete = o;
                }

                if (srvcToDelete != null) {
                    showNoticeDialog();
                }else{
                    Toast.makeText(
                            getApplicationContext(),
                            "You can't re-unsubscribe " + selectedServiceName,
                            Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });
    }

    /*
        onResume: Check Android document on Activity for more detailed discuss of different states
        of activity. Long story short, whenever the app reaches the page either from other page or
        from the dialog, this method would be called.
     */
    public void onResume(){
        super.onResume();

        srvcPkgAL.clear();
        try {
            customerServices.fetchIfNeeded();
        }catch (ParseException e){}
        srvcPkgAL.addAll((ArrayList<ParseObject>) customerServices.get("ServicesList"));
        srvcPkgAL.addAll((ArrayList<ParseObject>) customerServices.get("PackagesList"));
        for (ParseObject o: srvcPkgAL){
            try {
                o.fetchIfNeeded();
            }catch (ParseException e){}
        }
        adapter.notifyDataSetChanged();
        selectedServiceName = null;
    }

    //Call this method to pop the confirmation dialog.
    public void showNoticeDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new DeleteServiceConfirmDialogFragment();
        dialog.show(getSupportFragmentManager(), "DeleteServiceConfirmDialogFragment");
    }

    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    // defined by the NoticeDialogFragment.NoticeDialogListener interface
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        customerServices.removeService(srvcToDelete);
        customerServices.saveInBackground();
        Toast.makeText(
                getApplicationContext(),
                "You unsubscribed " + selectedServiceName,
                Toast.LENGTH_LONG).show();

        //currentUser.fetchInBackground();

        //Direct callback of onResume to refresh UI.
        this.onResume();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
        // Nothing happens, sun still rises
        this.onResume();
    }
}