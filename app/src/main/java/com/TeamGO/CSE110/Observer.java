package com.TeamGO.CSE110;

/**
 * Created by flyfire2002 on 03/08/2015.
 */
public interface Observer {
    void addSubject(Services subject);
    void update();
}
