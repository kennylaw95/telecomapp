package com.TeamGO.CSE110;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SignUpCallback;


public class CommercialSignUpActivity extends Activity {

    // Declare Variables
    static boolean signupSuccess;

    Button signup;



    //Textbox Labels
    String companytxt;
    String responsiblePartytxt;
    String addresstxt;
    String usernametxt;
    String passwordtxt;
    String passwordConfirmtxt;
    String phonetxt;


    //Empty Text Entry Forms
    EditText company;
    EditText responsibleParty;

    EditText phone;
    EditText name;
    EditText address;
    EditText password;
    EditText username;
    EditText passwordConfirm;

    /** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get the view from layout xml
        setContentView(R.layout.activity_commercial_sign_up);

        // Locate EditTexts in layout xml
        company = (EditText) findViewById(R.id.companyname);
        responsibleParty = (EditText) findViewById(R.id.personResponsible);
        address = (EditText) findViewById(R.id.address);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        passwordConfirm = (EditText) findViewById(R.id.passwordConfirm);
        phone = (EditText) findViewById(R.id.phone);

        // Locate Buttons in xml
        signup = (Button) findViewById(R.id.signup);

        signupSuccess = false;

        // Sign up Button Click Listener
        signup.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                // Retrieve the text entered from the EditText
                usernametxt = username.getText().toString();
                passwordtxt = password.getText().toString();
                companytxt = company.getText().toString();
                responsiblePartytxt = responsibleParty.getText().toString();
                addresstxt = address.getText().toString();
                passwordConfirmtxt = passwordConfirm.getText().toString();
                phonetxt = phone.getText().toString();

                // Force user to fill out all fields of form
                if (usernametxt.equals("") || passwordtxt.equals("")
                        ||companytxt.equals("") || addresstxt.equals("") || phonetxt.equals("")
                        || responsiblePartytxt.equals("")) {

                    Toast.makeText(getApplicationContext(),
                            "Please complete the sign up form",
                            Toast.LENGTH_LONG).show();
                } else if(!passwordtxt.equals(passwordConfirmtxt)) {
                    Toast.makeText(getApplicationContext(),
                            "Passwords do not match!",
                            Toast.LENGTH_LONG).show();
                } else {
                    // Save new user data into Parse.com Data Storage
                    final CommercialCustomer commercialCustomer = ParseObject.create(CommercialCustomer.class);
                    commercialCustomer.setUsername(usernametxt);
                    commercialCustomer.setPassword(passwordtxt);
                    commercialCustomer.setAddress(addresstxt);
                    commercialCustomer.setCompany(companytxt);
                    commercialCustomer.setResponsibleParty(responsiblePartytxt);
                    commercialCustomer.put("Usertype", 3);
                    commercialCustomer.setNumber(phonetxt);

                    commercialCustomer.signUpInBackground(new SignUpCallback() {
                        public void done(ParseException e) {
                            if (e == null) {
                                //Create the services status entry for the user in DB

                                //create services object which holds member services info
                                Services customerServices = ParseObject.create(Services.class);
                                customerServices.setMember(usernametxt);
                                customerServices.init();
                                customerServices.addObserver(commercialCustomer);

                                try{
                                    customerServices.save();
                                    customerServices.fetchIfNeeded();
                                    commercialCustomer.addSubject(customerServices);
                                    commercialCustomer.save();
                                }catch(ParseException pe){}
                                signupSuccess = true;

                                //pull up login screen
                                Intent intent = new Intent(
                                        CommercialSignUpActivity.this,
                                        LoginSignupActivity.class);
                                startActivity(intent);
                                // Show a simple Toast message upon successful registration
                                Toast.makeText(getApplicationContext(),
                                        "Successfully signed up. Please log in.",
                                        Toast.LENGTH_LONG).show();
                                //Upon successful login, kill the SignUpOptionActivity.
                                if (SignUpOptionActivity.SUOA != null)
                                    SignUpOptionActivity.SUOA.finish();
                                finish();
                            }
                            //handle duplicate username case
                            else if(e.getCode() == 202){
                                Toast.makeText(getApplicationContext(),
                                        "Username is already taken. Choose a different username.",
                                        Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(getApplicationContext(),
                                        "Sign up Error"+ e.getCode(), Toast.LENGTH_LONG)
                                        .show();
                            }
                        }
                    });
                }


            }
        });

    }

    public static boolean isSignupSuccess(){
        return signupSuccess;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
