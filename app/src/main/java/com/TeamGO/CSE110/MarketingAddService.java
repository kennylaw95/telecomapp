package com.TeamGO.CSE110;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Created by Jeffery on 2/22/15.
 */
public class MarketingAddService extends Activity {
    //to hold entered values
    String internetTxt;
    String internetPriceTxt;
    String internetDurationTxt;

    String mobileTxt;
    String mobilePriceTxt;
    String mobileDurationTxt;

    String homeTxt;
    String homePriceTxt;
    String homeDurationTxt;
    //for the three buttons on our page
    Button internetButton;
    Button mobileButton;
    Button homeButton;

    //for EditText values
    EditText internetName;
    EditText internetPrice;
    EditText internetDuration;

    EditText mobileName;
    EditText mobilePrice;
    EditText mobileDuration;

    EditText homeName;
    EditText homePrice;
    EditText homeDuration;



    // Locate EditTexts in layout xml
/*
    serviceName = (EditText) findViewById(R.id.name);
    address = (EditText) findViewById(R.id.address);
    username = (EditText) findViewById(R.id.username);
    password = (EditText) findViewById(R.id.password);
    passwordConfirm = (EditText) findViewById(R.id.passwordConfirm);
*/

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.marketing_add_service);

        internetName = (EditText) findViewById(R.id.iNameField);
        internetPrice = (EditText) findViewById(R.id.iPriceField);
        internetDuration = (EditText) findViewById(R.id.iDurationField);

        mobileName = (EditText) findViewById(R.id.mNameField);
        mobilePrice = (EditText) findViewById(R.id.mPriceField);
        mobileDuration = (EditText) findViewById(R.id.mDurationField);

        homeName = (EditText) findViewById(R.id.hNameField);
        homePrice = (EditText) findViewById(R.id.hPriceField);
        homeDuration = (EditText) findViewById(R.id.hDurationField);

        //Locate Buttons in XML
        internetButton = (Button) findViewById(R.id.internetButton);
        mobileButton = (Button) findViewById(R.id.mobileButton);
        homeButton = (Button) findViewById(R.id.homeButton);

        //for internet Service
        internetButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {

                // Retrieve the text entered from the EditText
                internetTxt = internetName.getText().toString();
                internetPriceTxt = internetPrice.getText().toString();
                internetDurationTxt = internetDuration.getText().toString();
                //test for empty input
                if(internetTxt.equals("") || internetPriceTxt.equals("") ||
                internetDurationTxt.equals("")) {
                    Toast.makeText(getApplicationContext(),
                            "Entered empty input!",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                //add new internet service to all services
                InternetService internet = ParseObject.create(InternetService.class);

                internet.setPrice(Double.parseDouble(internetPriceTxt));
                internet.setSubscriptionPeriod(Integer.parseInt(internetDurationTxt));
                internet.setName(internetTxt);
                internet.setAvailability(true);

                internet.saveInBackground();

                internetName.setText("");
                internetPrice.setText("");
                internetDuration.setText("");
                Toast.makeText(getApplicationContext(),
                        "New Service "+internetTxt+" Added!",
                        Toast.LENGTH_LONG).show();
            }
        });

        //for mobile Service
        mobileButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {

                // Retrieve the text entered from the EditText
                mobileTxt = mobileName.getText().toString();
                mobilePriceTxt = mobilePrice.getText().toString();
                mobileDurationTxt = mobileDuration.getText().toString();

                //test for empty input
                if(mobileTxt.equals("") || mobilePriceTxt.equals("") ||
                        mobileDurationTxt.equals("")) {
                    Toast.makeText(getApplicationContext(),
                            "Entered empty input!",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                //add new mobile service to all services
                WirelessService mobile = ParseObject.create(WirelessService.class);

                mobile.setPrice(Double.parseDouble(mobilePriceTxt));
                mobile.setSubscriptionPeriod(Integer.parseInt(mobileDurationTxt));
                mobile.setName(mobileTxt);
                mobile.setAvailability(true);

                mobile.saveInBackground();


                mobileName.setText("");
                mobilePrice.setText("");
                mobileDuration.setText("");
                Toast.makeText(getApplicationContext(),
                        "New Service "+mobileTxt+" Added!",
                        Toast.LENGTH_LONG).show();
            }
        });

        //for mobile Service
        homeButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {

                // Retrieve the text entered from the EditText
                homeTxt = homeName.getText().toString();
                homePriceTxt = homePrice.getText().toString();
                homeDurationTxt = homeDuration.getText().toString();


                //test for empty input
                if(homeTxt.equals("") || homePriceTxt.equals("") ||
                        homeDurationTxt.equals("")) {
                    Toast.makeText(getApplicationContext(),
                            "Entered empty input!",
                            Toast.LENGTH_LONG).show();
                    return;
                }


                //add new mobile service to all services
                WirelineService homephone = ParseObject.create(WirelineService.class);

                homephone.setPrice(Double.parseDouble(homePriceTxt));
                homephone.setSubscriptionPeriod(Integer.parseInt(homeDurationTxt));
                homephone.setName(homeTxt);
                homephone.setAvailability(true);

                homephone.saveInBackground();

                homeName.setText("");
                homePrice.setText("");
                homeDuration.setText("");

                Toast.makeText(getApplicationContext(),
                        "New Service "+homeTxt+" Added!",
                        Toast.LENGTH_LONG).show();
            }
        });
    }
}
