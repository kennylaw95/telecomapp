package com.TeamGO.CSE110;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Law on 2/22/15.
 */
public class MyBillActivity extends Activity {
    String thresholdTxt = "0";

    TextView billLabel;
    TextView billText;

    TextView topLabel;
    EditText entered;
    Button submit;


    ParseObject services;

    String billDisplay= "";
    double price;
    String service;

    Services thisOne;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from singleitemview.xml
        setContentView(R.layout.bill_activity);

        topLabel = (TextView)findViewById(R.id.enterLabel);
        entered = (EditText)findViewById(R.id.enterThreshold);
        submit = (Button)findViewById(R.id.buttonSubmit);


        billLabel = (TextView)findViewById(R.id.billLabel);
        billText = (TextView)findViewById(R.id.billText);


        // Retrieve current user from Parse.com
        final ParseUser currentUser = ParseUser.getCurrentUser();
        final String userName = currentUser.getUsername();

        ParseQuery<Services> query = new ParseQuery<Services>("ServicesStatus");

        //kenny code
        query.findInBackground(new FindCallback<Services>() {
            public void done(List<Services> objs, ParseException e) {
                if (e == null) {
                    for (Services o : objs) {
                                                //Find the pairing serviceStatus entry.
                        if (o.get("Username").equals(userName)) {
                            ArrayList<ParseObject> servicesList = (ArrayList<ParseObject>) o.get("ServicesList");
                            ArrayList<ParseObject> packagesList = (ArrayList<ParseObject>) o.get("PackagesList");
                            thisOne = o;
                            //double sum = 0;
                            for (ParseObject obj : servicesList)
                            {
                                /*Toast.makeText(getApplicationContext(),
                                        (String)obj.get("Name"),
                                        Toast.LENGTH_LONG).show();*/
                                try {
                                    price = obj.fetchIfNeeded().getDouble("Price");
                                    //sum += price;
                                    service = obj.fetchIfNeeded().getString("Name");
                                    billDisplay+= service + "      " + price + "\n";
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                }
                            }
                            for (ParseObject obj : packagesList)
                            {
                                try {
                                    price= obj.fetchIfNeeded().getDouble("Cost");
                                    //sum += price;
                                    service = obj.fetchIfNeeded().getString("Name");
                                    billDisplay+= service + "      " + price + "\n";
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                }
                            }
                            billDisplay+="------------------\n" + "total      "+ o.getDouble("Bill");
                            //o.put("Bill", sum);
                            //o.saveInBackground();
                            billText.setText(billDisplay);
                            onResume(); //Refresh UI.c
                        }
                    }
                } else {}
            }
        });

        //for threshold submission
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                thresholdTxt = entered.getText().toString();

                //for empty input
                if(thresholdTxt == "") {
                    Toast.makeText(getApplicationContext(),
                            "Entered empty input!",
                            Toast.LENGTH_LONG).show();
                            return;
                }
                double value = Double.parseDouble(thresholdTxt);
                thisOne.setThreshold(value);
                try{
                    thisOne.save();
                } catch(ParseException e) {

                }


            }
        });

        billLabel.setText("Current Bill for " + userName);
    }
}
