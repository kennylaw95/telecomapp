package com.TeamGO.CSE110;

import android.content.Intent;

import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ParseObject;

import java.util.List;
import java.util.ArrayList;

/**
 * Created by Law on 3/10/15.
 */
public class TelecomCompany {
    //private Creator creator;
    List<ParseUser> user;
    List<ParseObject> availSrvcPkgAL;
    RuleObject ruleObject;
    MarketingAddService addService;


    public TelecomCompany()
    {
        user = new ArrayList<ParseUser>();
        availSrvcPkgAL = new ArrayList<ParseObject>();
        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
        try {
            user = userQuery.find();
        } catch(ParseException e){}

        ParseQuery<InternetService> internetQuery = new ParseQuery<InternetService>("Internet");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<InternetService> objs = internetQuery.find();
            for (InternetService o : objs) {
                if (!availSrvcPkgAL.contains(o) && o.getAvailability())
                    availSrvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<WirelessService> wirelessQuery = new ParseQuery<WirelessService>("Wireless");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelessService> objs = wirelessQuery.find();
            for (WirelessService o : objs) {
                if (!availSrvcPkgAL.contains(o) && o.getAvailability())
                    availSrvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<WirelineService> wirelineQuery = new ParseQuery<WirelineService>("Wireline");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelineService> objs = wirelineQuery.find();
            for (WirelineService o : objs) {
                if (!availSrvcPkgAL.contains(o) && o.getAvailability())
                    availSrvcPkgAL.add(o);
            }
        }catch (ParseException e){} ruleObject = null;

        ParseQuery<Packages> packageQuery = new ParseQuery<Packages>("Packages");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<Packages> objs = packageQuery.find();
            for (Packages o : objs) {
                if (!availSrvcPkgAL.contains(o))
                    availSrvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        addService = new MarketingAddService();
    }

    /**
     * Name : billCustomer
     * Description: This method is implemented in our Facade class
     *              in order to send the bill to the customer.
     *              It starts a new intent leading it to the
     *              bill activity page that will display the current
     *              bill of the user as well as send a notification
     * Parameters: none
     * Return: void
     */
    public void billCustomer()
    {
        Intent intent = new Intent(String.valueOf(MyBillActivity.class));
    }

    /**
     * Name : createService
     * Description: This method is implemented in our Facade class
     *              in order to create a service on our system.
     *              It starts a new intent leading it to the
     *              add service page that will create services on
     *              our database. It is our factory that will
     *              create different services depending on the type
     *              that the user selects and wants to create.
     * Parameters: none
     * Return: void
     */
    public void createService()
    {
        Intent intent = new Intent(String.valueOf(MarketingAddService.class));
    }

    /**
     * Name : discontinueService
     * Description: This method is implemented in our Facade class
     *              in order to delete a service on our system.
     *              It starts a new intent leading it to the
     *              add service page that will delete services on
     *              our database. It is our factory that will
     *              create different services depending on the type
     *              that the user selects and wants to create.
     * Parameters: none
     * Return: void
     */
    public void discontinueService()
    {
        Intent intent = new Intent(String.valueOf(MarketingRepRemoveServices.class));
    }
}
