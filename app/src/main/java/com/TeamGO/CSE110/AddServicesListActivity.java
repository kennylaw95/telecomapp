package com.TeamGO.CSE110;

/**
 * Created by Jeffery on 2/8/15.
 */
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.TeamGO.CSE110.R;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class AddServicesListActivity extends Activity {

    ParseUser currentUser;
    MediaPlayer whee;
    ImageView image;
    Customer customer;
    Services customerServices;
    String username;// used to associate user with service
    private CheckBox chkWireless, chkWireline, chkInternet;
    private Button addBtn;

    StringBuffer result = new StringBuffer(); //provides user feedback that service was added

    //Dynamic list related vars
    String selectedServiceName;
    ListView serviceListView;
    ArrayAdapter<ParseObject> adapter;
    Button addServiceButton;
    Services services;
    ArrayList<ParseObject> srvcPkgAL;
    MediaPlayer addedService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_services_list_activity_layout);

        whee = MediaPlayer.create(this, R.raw.whee);

        image = (ImageView) findViewById(R.id.imageView);
        //Since the user is not dynamic, we get the user and service status on create.
        currentUser = ParseUser.getCurrentUser();
        username = currentUser.getUsername();

        if (currentUser.getInt("Usertype") == 0) {
            customer = ParseObject.createWithoutData(RetailCustomer.class, currentUser.getObjectId());
        }else{
            customer = ParseObject.createWithoutData(CommercialCustomer.class, currentUser.getObjectId());
        }

        /*Dynamic List part*/
        srvcPkgAL = new ArrayList<ParseObject>();//Here one should specify the type of object
                                            //getting stored. In actual implementation it would
                                            //be either service or package
        serviceListView = (ListView) findViewById(R.id.servicelistview);//Get the listview

        /*Get the list of all services and packages*/
        ParseQuery<InternetService> internetQuery = new ParseQuery<InternetService>("Internet");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<InternetService> objs = internetQuery.find();
            for (InternetService o : objs) {
                if (!srvcPkgAL.contains(o) && o.getAvailability())
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<WirelessService> wirelessQuery = new ParseQuery<WirelessService>("Wireless");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelessService> objs = wirelessQuery.find();
            for (WirelessService o : objs) {
                if (!srvcPkgAL.contains(o) && o.getAvailability())
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<WirelineService> wirelineQuery = new ParseQuery<WirelineService>("Wireline");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelineService> objs = wirelineQuery.find();
            for (WirelineService o : objs) {
                if (!srvcPkgAL.contains(o) && o.getAvailability())
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<Packages> packageQuery = new ParseQuery<Packages>("Packages");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<Packages> objs = packageQuery.find();
            for (Packages o : objs) {
                if (!srvcPkgAL.contains(o))
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        adapter = new ArrayAdapter<ParseObject>(this,
                android.R.layout.simple_list_item_1, srvcPkgAL);
        //Setup the adapter between the listview and the array.
        //First arg should be the activity, second leave it as is, third one the ArrayList
        //storing the object you would like to display.
        serviceListView.setAdapter(adapter);
        //Attach the adapter to the listview

        serviceListView.setOnItemClickListener( //Set up click listener. It is trigger if a item in the list
                //is clicked.
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView parent, View v, int position, long id) {
                        selectedServiceName = (String)((TextView)v).getText();
                        //All item in the listview is a TextView created by calling the toString() of
                        //each object in the forementioned ArrayList.I had overrided the toString of
                        //different services classes
                    }
                });

        //Set up the button
        addServiceButton = (Button) findViewById(R.id.servicelistbutton);
        addServiceButton.setOnClickListener(new OnClickListener() {
            //In this test I make the button act like a deleter, deleting the item selected.
            @Override
            public void onClick(View v) {
                if (selectedServiceName == null){
                    Toast.makeText(
                            getApplicationContext(),
                            "You haven't selected anything",
                            Toast.LENGTH_LONG).show();
                    return;
                }
                ArrayList<ParseObject> srcList = (ArrayList<ParseObject>) customerServices.get("ServicesList");
                for (ParseObject o: srcList){
                    String srvcName = null;
                    try{
                        srvcName = o.fetchIfNeeded().getString("Name");
                    }catch (ParseException e){}

                    if (srvcName.equals(selectedServiceName)){
                        Toast.makeText(
                                getApplicationContext(),
                                "You can't resubscribe " + selectedServiceName,
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                ParseObject srvcToAdd = null;
                for(ParseObject o: srvcPkgAL){
                    if (o.getString("Name").equals(selectedServiceName))
                        srvcToAdd = o;
                }
                if (srvcToAdd != null) {
                    customerServices.addService(srvcToAdd);
                    Toast.makeText(
                            getApplicationContext(),
                            ""+ selectedServiceName + " is subscribed",
                            Toast.LENGTH_SHORT).show();
                    try{
                        customerServices.save();
                    }catch (ParseException e){}
                }
                image.setImageResource(R.drawable.addedservice);

                whee.start();


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        ImageView image = (ImageView)findViewById(R.id.imageView);
                        image.setImageResource(R.drawable.addservice);
                    }
                }, 10000);


                onResume();
            }
        });
/*Dynamic List part -- END*/


        // QUERY THE SERVICES OBJECTS
        ParseQuery<Services> query = new ParseQuery<Services>("ServicesStatus");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<Services> objs = query.find();
            for (Services o : objs) {
                if (o.get("Username").equals(currentUser.getUsername())) {
                    services = o;
                    customerServices = ParseObject.createWithoutData(Services.class, services.getObjectId());
                    break;

                }
            }
        }catch (ParseException e){}


    }


    @Override
    public void onResume(){
        super.onResume();
        try{
            customerServices.fetchIfNeeded();
        }catch (ParseException e){}
    }


}
