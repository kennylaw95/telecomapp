package com.TeamGO.CSE110;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;


public class MarketingRepRemoveServices extends FragmentActivity
        implements MRRemoveServiceConfirmDialogFragment.MRRemoveServiceConfirmDialogListener {

    ParseObject srvcToDelete;


    String selectedServiceName;
    ListView serviceListView;
    ArrayAdapter<ParseObject> adapter;
    Button deleteServiceButton;
    ArrayList<ParseObject> srvcPkgAL;

    /*
        onCreate: Init the UI, set up button onClickListeners
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from layout xml
        setContentView(R.layout.activity_marketing_rep_remove_services);


        srvcPkgAL = new ArrayList<ParseObject>();//Here one should specify the type of object
        //getting stored. In actual implementation it would
        //be either service or package
        serviceListView = (ListView) findViewById(R.id.myservicelistview);//Get the listview

        /*Get the list of all services and packages*/
        ParseQuery<InternetService> internetQuery = new ParseQuery<InternetService>("Internet");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<InternetService> objs = internetQuery.find();
            for (InternetService o : objs) {
                if (!srvcPkgAL.contains(o) && o.getAvailability())
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<WirelessService> wirelessQuery = new ParseQuery<WirelessService>("Wireless");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelessService> objs = wirelessQuery.find();
            for (WirelessService o : objs) {
                if (!srvcPkgAL.contains(o) && o.getAvailability())
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<WirelineService> wirelineQuery = new ParseQuery<WirelineService>("Wireline");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelineService> objs = wirelineQuery.find();
            for (WirelineService o : objs) {
                if (!srvcPkgAL.contains(o) && o.getAvailability())
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<Packages> packageQuery = new ParseQuery<Packages>("Packages");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<Packages> objs = packageQuery.find();
            for (Packages o : objs) {
                if (!srvcPkgAL.contains(o))
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        adapter = new ArrayAdapter<ParseObject>(this,
                android.R.layout.simple_list_item_1, srvcPkgAL);
        //Setup the adapter between the listview and the array.
        //First arg should be the activity, second leave it as is, third one the ArrayList
        //storing the object you would like to display.
        serviceListView.setAdapter(adapter);
        //Attach the adapter to the listview

        serviceListView.setOnItemClickListener( //Set up click listener. It is trigger if a item in the list
                //is clicked.
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView parent, View v, int position, long id) {
                        selectedServiceName = (String)((TextView)v).getText();
                        //All item in the listview is a TextView created by calling the toString() of
                        //each object in the forementioned ArrayList.I had overrided the toString of
                        //different services classes
                    }
                });
        deleteServiceButton = (Button) findViewById(R.id.deletebutton);
        deleteServiceButton.setOnClickListener(new View.OnClickListener() {
            //In this test I make the button act like a deleter, deleting the item selected.
            @Override
            public void onClick(View v) {
                if (selectedServiceName == null){
                    Toast.makeText(
                            getApplicationContext(),
                            "You haven't selected anything",
                            Toast.LENGTH_LONG).show();
                    return;
                }




                for(ParseObject o: srvcPkgAL){
                    if (o.getString("Name").equals(selectedServiceName))
                        srvcToDelete = o;
                }

                    showNoticeDialog();

            }
        });
    }

    /*
        onResume: Check Android document on Activity for more detailed discuss of different states
        of activity. Long story short, whenever the app reaches the page either from other page or
        from the dialog, this method would be called.
     */
    public void onResume(){
        super.onResume();

        srvcPkgAL.clear();
/*Get the list of all services and packages*/
        ParseQuery<InternetService> internetQuery = new ParseQuery<InternetService>("Internet");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<InternetService> objs = internetQuery.find();
            for (InternetService o : objs) {
                if (!srvcPkgAL.contains(o) && o.getAvailability())
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<WirelessService> wirelessQuery = new ParseQuery<WirelessService>("Wireless");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelessService> objs = wirelessQuery.find();
            for (WirelessService o : objs) {
                if (!srvcPkgAL.contains(o) && o.getAvailability())
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<WirelineService> wirelineQuery = new ParseQuery<WirelineService>("Wireline");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelineService> objs = wirelineQuery.find();
            for (WirelineService o : objs) {
                if (!srvcPkgAL.contains(o) && o.getAvailability())
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}

        ParseQuery<Packages> packageQuery = new ParseQuery<Packages>("Packages");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<Packages> objs = packageQuery.find();
            for (Packages o : objs ) {
                if (!srvcPkgAL.contains(o) && o.getAvailability())
                    srvcPkgAL.add(o);
            }
        }catch (ParseException e){}
        for (ParseObject o: srvcPkgAL){
            try {
                o.fetchIfNeeded();
            }catch (ParseException e){}
        }
        adapter.notifyDataSetChanged();
        selectedServiceName = null;
    }

    //Call this method to pop the confirmation dialog.
    public void showNoticeDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new MRRemoveServiceConfirmDialogFragment();
        dialog.show(getSupportFragmentManager(), "MRRemoveServiceConfirmDialogFragment");
    }

    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    // defined by the NoticeDialogFragment.NoticeDialogListener interface
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        try {
            srvcToDelete.fetchIfNeeded().put("Available", false);
            srvcToDelete.save();
        }catch (ParseException e){}
        //srvcToDelete.put("Available", false);

        Toast.makeText(

                getApplicationContext(),
                "You removed " + selectedServiceName,
                Toast.LENGTH_LONG).show();

        //currentUser.fetchInBackground();

        //Direct callback of onResume to refresh UI.
        this.onResume();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
        // Nothing happens, sun still rises
        this.onResume();
    }
}
