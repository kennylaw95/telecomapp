package com.TeamGO.CSE110;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
//import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SignUpOptionActivity extends Activity {
	public static Activity SUOA; //Define a static Activity instance
	Button retailSignup;
    Button commercialSignup;

	/* When activity is first created */
	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
		 	SUOA = this; //Assign that static Act instance to be this.
		 				 //To be used in SignUp activities to finish this activity. 
	        super.onCreate(savedInstanceState);
	        /* set layout */
	        setContentView(R.layout.activity_sign_up_options);
	        
	        //Show up navigation
	        getActionBar().setDisplayHomeAsUpEnabled(true);
	        
	        retailSignup = (Button) findViewById(R.id.retail_signup);
            commercialSignup = (Button) findViewById(R.id.commercial_signup);
	        
			// retail sign up Button Click Listener
			retailSignup.setOnClickListener(new OnClickListener() {
	 
				public void onClick(View arg0) {
	
					//Direct to retail sign up page to fill in required information
					Intent intent = new Intent(
							SignUpOptionActivity.this,
							RetailSignUpActivity.class);
					startActivity(intent);
					
				}
			});
            commercialSignup.setOnClickListener(new OnClickListener() {

             public void onClick(View arg0) {

                 //Direct to retail sign up page to fill in required information
                 Intent intent = new Intent(
                         SignUpOptionActivity.this,
                         CommercialSignUpActivity.class);
                 startActivity(intent);

             }
         });
	 
		}

	    

	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        switch (item.getItemId()) {
	        // Respond to the action bar's Up/Home button
	        case android.R.id.home:
	          //  NavUtils.navigateUpFromSameTask(this);
	            return true;
	        }
	        return super.onOptionsItemSelected(item);
	    }
}
