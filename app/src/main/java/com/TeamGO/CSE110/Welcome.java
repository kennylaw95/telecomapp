package com.TeamGO.CSE110;
 
import com.TeamGO.CSE110.R;
import com.parse.ParseUser;
import com.parse.ParseQuery;
import com.parse.FindCallback;
import com.parse.ParseException;
import java.util.List;

import android.content.Intent;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Welcome extends Activity {
 
	// Declare Variable
    Button myServices;
    Button viewBill;
    Button addServices;
	Button logout;

    TextView userinfo;
    TextView addressText;
    TextView phoneText;
    TextView businessName;

    TextView accountT;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from singleitemview.xml
		setContentView(R.layout.welcome);

		// Retrieve current user from Parse.com
		ParseUser currentUser = ParseUser.getCurrentUser();
 
		// Convert currentUser into String
		String struser = currentUser.getUsername().toString();

        String address = currentUser.getString("address");

        String phoneNumber = currentUser.getString("PhoneNumber");

        accountT = (TextView)findViewById(R.id.AccountType);
        if(currentUser.getInt("Usertype") == 3) {
                accountT.setText("Commerical Account");
        } else {
                accountT.setText("Retail Account");
        }

        userinfo = (TextView)findViewById(R.id.username);
        addressText = (TextView)findViewById(R.id.address);
        phoneText = (TextView) findViewById(R.id.phoneNumber);
        businessName = (TextView) findViewById(R.id.bName);

        userinfo.setText("Username: " +  struser);
        addressText.setText("Address: " + address);
        phoneText.setText("Phone #: " + phoneNumber);
        if(currentUser.getInt("Usertype") == 3 ) {
                businessName.setText("Company Name: " + currentUser.getString("Company"));
        } else {
                //nothing
        }
        // Locate TextView in welcome.xml
		TextView txtuser = (TextView) findViewById(R.id.txtuser);
 
		// Set the currentUser String into TextView
		txtuser.setText("You are logged in as " + struser);

        //Locate Button in welcome.xml
        myServices = (Button) findViewById(R.id.myServices);

        // myServices Button Click Listener
        myServices.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // Logout current user
                Intent intent1 = new Intent(
                        Welcome.this,
                        MyServicesListActivity.class);

                startActivity(intent1);

            }
        });

        //Locate Button in welcome.xml
        viewBill = (Button) findViewById(R.id.billButton);

        // addServices Button Click Listener
        viewBill.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                // Logout current user
                Intent intent = new Intent(
                        Welcome.this,
                        MyBillActivity.class);
                startActivity(intent);

            }
        });

        //Locate Button in welcome.xml
        addServices = (Button) findViewById(R.id.addServices);

        // addServices Button Click Listener
        addServices.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                // Logout current user
                Intent intent = new Intent(
                        Welcome.this,
                        AddServicesListActivity.class);
                startActivity(intent);

            }
        });



		// Locate Button in welcome.xml
		logout = (Button) findViewById(R.id.logout);
 
		// Logout Button Click Listener
		logout.setOnClickListener(new OnClickListener() {
 
			public void onClick(View arg0) {
				// Logout current user
				ParseUser.logOut();
				finish();
			}
		});




	}
}