package com.TeamGO.CSE110;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SignUpCallback;

import java.util.ArrayList;
import java.util.List;


public class CreatePackageActivity extends Activity {

    Button createPkgBtn;

    //Textbox labels
    String packageNametxt;
    int durationtxt;
    double pricetxt;

    //Empty textbox forms
    EditText packageName;
    EditText duration;
    EditText price;

    ListView serviceListView;


    Packages newPackage = ParseObject.create(Packages.class);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_package);



        //show the internet list
        serviceListView = (ListView) findViewById(R.id.serviceList);
        //serviceListView.setSelector(R.drawable.list_selector);
        newPackage.put("Available", true);
        //define array values to show in listview

        ArrayList<ParseObject> services = new ArrayList<ParseObject>();

        //add internet services to services list
        ParseQuery<ParseObject> internetQuery = new ParseQuery<ParseObject>("Internet");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<ParseObject> objs = internetQuery.find();
            for (ParseObject o : objs) {
                if(o.getBoolean("Available")){services.add(o);}
            }
        }catch (ParseException e){}

        //add wireless services to services list
        ParseQuery<ParseObject> wirelessQuery = new ParseQuery<ParseObject>("Wireless");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<ParseObject> objs = wirelessQuery.find();
            for (ParseObject o : objs) {
                if(o.getBoolean("Available")){services.add(o);}
            }
        }catch (ParseException e){}

        //add wireline services to services list
        ParseQuery<ParseObject> wirelineQuery = new ParseQuery<ParseObject>("Wireline");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<ParseObject> objs = wirelineQuery.find();
            for (ParseObject o : objs) {
                if(o.getBoolean("Available")){services.add(o);}
            }
        }catch (ParseException e){}

        //define new adapter
        ArrayAdapter<ParseObject>Adapter = new ArrayAdapter<ParseObject>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, services);

        //assign adapter to list view
        serviceListView.setAdapter(Adapter);

        //ListView item click Listener
        serviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //listview clicked item index
              /*  int itemPosition = position;
                //ListView Clicked item value
                String itemValue = (String)internetListView.getItemAtPosition(position);

                //add service object to package
            */
                serviceListView.setSelected(true);
                newPackage.addService((ParseObject)serviceListView.getItemAtPosition(position));
                //newPackage.saveInBackground();

            }
        });


        //locate texts
        packageName = (EditText) findViewById(R.id.packageName);
        price = (EditText) findViewById(R.id.price);
        duration = (EditText)findViewById(R.id.duration);

        createPkgBtn = (Button) findViewById(R.id.createPackageBtn);

        createPkgBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                // Retrieve the text entered from the EditText
                packageNametxt = packageName.getText().toString();


                // Force user to fill out all fields of form and check for valid input
                if (packageNametxt.equals("")
                        || price.getText().toString().equals("")
                        || duration.getText().toString().equals("")
                   ) {

                    Toast.makeText(getApplicationContext(),
                            "Please fill in all fields and select at least one service to add",
                            Toast.LENGTH_LONG).show();
                } else {
                    pricetxt = Double.parseDouble(price.getText().toString());
                    durationtxt = Integer.parseInt(duration.getText().toString());
                    newPackage.setCost(pricetxt);
                    newPackage.setDuration(durationtxt);
                    newPackage.setName(packageNametxt);
                    newPackage.saveInBackground();
                }




            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_package, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
