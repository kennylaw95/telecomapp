package com.TeamGO.CSE110;

/**
 * Created by flyfire2002 on 02/09/2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class MRRemoveServiceConfirmDialogFragment extends DialogFragment {
    //Declare a interface to allow the MyServicesListActivity to behave according to users'
    //choices on the dialog.
    public interface MRRemoveServiceConfirmDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    MRRemoveServiceConfirmDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    // activity is the host (MyServicesListActivity)
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (MRRemoveServiceConfirmDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement DeleteConfirmDialogListener");
        }
    }

    //Create the dialog when necessary, setup texts and buttons
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.RemoveConfirmMsg)
                .setPositiveButton(R.string.Remove, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Delegate to the host to react.
                        mListener.onDialogPositiveClick(MRRemoveServiceConfirmDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Same here
                        mListener.onDialogNegativeClick(MRRemoveServiceConfirmDialogFragment.this);
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
