package com.TeamGO.CSE110;

/**
 * Created by Jessica on 3/5/2015.
 */
public class Assessor {

    public boolean evaluate(Properties prop) {
        return prop.getCurrentBill() > prop.getBillThreshold();
    }


    public String getErrors() {
        return null;
    }
}
