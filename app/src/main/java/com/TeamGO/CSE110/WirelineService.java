package com.TeamGO.CSE110;

import com.parse.*;

/**
 * Created by Jeffery on 2/8/15.
 */
@ParseClassName("Wireline")
public class WirelineService extends ParseObject implements Service {
    // service members
    String name;
    String user; //owner of service
    double price;
    int subscriptionPeriod;

    public WirelineService() {
        name = "Wireline";
        user = null; //to be initialized upon addWireline();
        price = 50.00;
        // default
        subscriptionPeriod = 12;

    }
    @Override
    public void setAvailability(boolean availability) {
        put("Available", availability);
    }

    @Override
    public boolean getAvailability() {

        return getBoolean("Available");
    }
    public double getPrice() {
        return getDouble("Price");
    }

    public void setPrice(double price) {

        // context: app shows list of prices
        //if (price != /* any of the prices listed */)
        //{
            // User error message saying you entered in invalid
        //}

        put("Price", price);
    }

    public String getName(){
        return getString("Name");
    }
    public void setName(String name) {
        put("Name", name);
    }
    public int getSubscriptionPeriod() {return getInt("Days");}
    public void setSubscriptionPeriod(int period) {put("Days", period);}

    @Override
    public String toString(){
        return getName();
    }
}
