package com.TeamGO.CSE110;
import com.parse.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Jessica on 2/22/2015.
 */
@ParseClassName("ServicesStatus")
public class Services extends ParseObject implements Subject {
    String member;
    ArrayList<ParseObject> Services = new ArrayList<ParseObject>();
    //ArrayList<Observer> observerList;

    //create one when the user signs up
    public Services(){


    }

    public void setMember( String member){
        put("Username", member);
    }
    public String getMember(){
        return getString("Username");
    }

    //adds item to array
    public void addService(ParseObject service){
        //query for member name

        ParseQuery<Packages> packageQuery = new ParseQuery<Packages>("Packages");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<Packages> objs = packageQuery.find();
            for (Packages o : objs) {
                 if (service.getString("Name").equals(o.getString("Name"))){
                     ArrayList<ParseObject> packagesList = (ArrayList<ParseObject>) get("PackagesList");
                     if (!(packagesList.contains(o))) {
                         addUnique("PackagesList", service);
                         put("Bill", getBill() + o.getCost());
                         notifyObserver();
                     }
                     return;
                 }
            }
            fetchIfNeeded();
        }catch (ParseException e){}
        ArrayList<ParseObject> servicesList = (ArrayList<ParseObject>) get("ServicesList");
        if (!(servicesList.contains(service))) {
            addUnique("ServicesList", service);
            put("Bill", getBill() + ((Service)service).getPrice());
            notifyObserver();
        }
    }

    public void init(){
        put("ServicesList", new ArrayList<ParseObject>());
        put("PackagesList", new ArrayList<ParseObject>());
        put("Bill", 0);
        put("BillThreshold", 0);
    }
    //delete service from array
    public void removeService(ParseObject service){
        ArrayList<ParseObject> servicesList = (ArrayList<ParseObject>) get("ServicesList");
        ArrayList<ParseObject> packagesList = (ArrayList<ParseObject>) get("PackagesList");

        if (servicesList.contains(service)){
            servicesList.remove(service);
            put("ServicesList", servicesList);
            put("Bill", getBill() - ((Service)service).getPrice() + 150);
            notifyObserver();
        }
        if (packagesList.contains(service)){
            packagesList.remove(service);
            put("PackagesList", packagesList);
            put("Bill", getBill() - ((Packages)service).getCost() + 150);
            notifyObserver();
        }
    }

    //puts array into user column
    public void setServices(ArrayList<ParseObject> services){
        put("ServicesList", Arrays.asList(services));
    }

    public double getBill(){
        return getDouble("Bill");
    }

    public double getThreshold(){
        return getDouble("BillThreshold");
    }

    public void setThreshold(double amount){
        put("BillThreshold", amount);
    }

    public void addObserver(Observer observer){
        addUnique("ObserverList", observer);
    }
    public void removeObserver(Observer observer){
        ArrayList<Observer> observerList = (ArrayList<Observer>) get("ObserverList");
        if (observerList.contains(observer)){
            observerList.remove(observer);
            put("ObserverList", observerList);
        }
    }
    public void notifyObserver(){
        ArrayList<Observer> observerList = (ArrayList<Observer>) get("ObserverList");
        if (observerList == null) return;
        for(Observer o: observerList){
            o.update();
        }
    }

}
