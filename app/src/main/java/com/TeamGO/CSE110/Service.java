package com.TeamGO.CSE110;

/**
 * Created by Law on 2/8/15.
 */
public interface Service {
    public void setAvailability(boolean availability);
    public boolean getAvailability();

    public double getPrice();

    public void setPrice(double price);

    public String getName();
    public void setName(String name);

    public int getSubscriptionPeriod();
    public void setSubscriptionPeriod(int period);

 }
