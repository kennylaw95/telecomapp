package com.TeamGO.CSE110;

/**
 * Created by flyfire2002 on 03/08/2015.
 */
public interface Subject {
    public void addObserver(Observer observer);
    public void removeObserver(Observer observer);
    public void notifyObserver();

    public double getBill();
    public double getThreshold();
}
