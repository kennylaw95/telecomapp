package com.TeamGO.CSE110.test;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.ListView;

import com.TeamGO.CSE110.*;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by flyfire2002 on 03/01/2015.
 */
public class MyServicesListActivityTest extends
        ActivityInstrumentationTestCase2<MyServicesListActivity> {

    private CountDownLatch lock = new CountDownLatch(1);
    private MyServicesListActivity currActivity;
    Button deleteBtn;
    ParseUser testUser;
    Services serviceStat, customerServices;
    ListView serviceListView;

    public MyServicesListActivityTest() throws InterruptedException {
        super(MyServicesListActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        Parse.initialize(getInstrumentation().getTargetContext().getApplicationContext(),
                "AMTvK2ZetdSyAHBIiVnZaf4UVdOcLAfCTGsZdnke",
                "yFBpW6GF61xbO8N9PT3qTbIDLuESuyzSXMGx2LHP");
        try{
            ParseUser.logIn("flyfire2002", "12345678");
            testUser = ParseUser.getCurrentUser();
        }catch (ParseException e){
            throw e;
        }

        currActivity = getActivity(); //Get the activity under test

        ParseQuery<Services> query = new ParseQuery<Services>("ServicesStatus");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<Services> objs = query.find();
            for (Services o : objs) {
                if (o.get("Username").equals(testUser.getUsername())) {
                    serviceStat = o;
                    customerServices = ParseObject.createWithoutData(Services.class, serviceStat.getObjectId());
                    break;
                }
            }
        }catch (ParseException e){}

        serviceListView = (ListView) currActivity.findViewById(com.TeamGO.CSE110.R.id.myservicelistview);
        deleteBtn = (Button) currActivity.findViewById(com.TeamGO.CSE110.R.id.deletebutton);

        customerServices.init();
        ParseQuery<InternetService> internetQuery = new ParseQuery<InternetService>("Internet");
        ParseQuery<Packages> packageQuery = new ParseQuery<Packages>("Packages");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<InternetService> srvcObjs = internetQuery.find();
            List<Packages> pkgObjs = packageQuery.find();
            customerServices.addService(srvcObjs.get(0));
            customerServices.addService(pkgObjs.get(0));
        }catch (ParseException e){}
        try {
            customerServices.save();
        }catch (ParseException e){}
    }

    public void testADeleteNone(){
        try {
            customerServices.fetchIfNeeded();
        }catch (ParseException e){}
        List<Services> A = ((List<Services>)customerServices.get("ServicesList"));
        deleteBtn.callOnClick();
        try {
            customerServices.fetchIfNeeded();
        }catch (ParseException e){}
        List<Services> B = ((List<Services>)customerServices.get("ServicesList"));
        assertTrue(A.equals(B));
    }

    public void testBDeleteOne() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                serviceListView.setSelection(0);
                serviceListView.performItemClick(serviceListView.getSelectedView(),
                        0, serviceListView.getItemIdAtPosition(0));
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
        ParseObject toBeDelete = (ParseObject)(serviceListView.getSelectedItem());
        deleteBtn.callOnClick();
        try {
            customerServices.fetchIfNeeded();
        }catch (ParseException e){}
        List<Services> A = ((List<Services>)customerServices.get("ServicesList"));
        assertFalse(A.contains(toBeDelete));
    }
}