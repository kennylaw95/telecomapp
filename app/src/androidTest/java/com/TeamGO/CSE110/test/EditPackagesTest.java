package com.TeamGO.CSE110.test;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.TeamGO.CSE110.*;
import com.TeamGO.CSE110.R;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by flyfire2002 on 03/02/2015.
 */
public class EditPackagesTest extends
        ActivityInstrumentationTestCase2<EditPackages> {
    /*Class var*/
    CountDownLatch lock = new CountDownLatch(1);

    Button editPkgBtn;
    Button commitChangesBtn;

    ListView packagesListView;
    ListView serviceListView;

    TextView packageName;
    EditText duration;
    EditText price;

    EditPackages currActivity;


    public EditPackagesTest() {
        super(EditPackages.class);
    }

    protected void setUp() throws Exception {
        Parse.initialize(getInstrumentation().getTargetContext().getApplicationContext(),
                "AMTvK2ZetdSyAHBIiVnZaf4UVdOcLAfCTGsZdnke",
                "yFBpW6GF61xbO8N9PT3qTbIDLuESuyzSXMGx2LHP");
        try {
            ParseUser.logIn("flyfire2002", "12345678");
        } catch (ParseException e) {
            throw e;
        }

        currActivity = getActivity();

        packagesListView = (ListView) currActivity.findViewById(com.TeamGO.CSE110.R.id.packageList);
        serviceListView = (ListView) currActivity.findViewById(com.TeamGO.CSE110.R.id.currentPackagesList);

        packageName = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.packageName);
        price = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.price);
        duration = (EditText)currActivity.findViewById(com.TeamGO.CSE110.R.id.duration);

        editPkgBtn = (Button) currActivity.findViewById(com.TeamGO.CSE110.R.id.editPackageBtn);
        commitChangesBtn = (Button) currActivity.findViewById(R.id.commitChangesBtn);
    }

    public void testANoSelection() throws InterruptedException{
        editPkgBtn.callOnClick();
        assertTrue(packageName.getText().toString().equals(""));
    }

    public void testBSelection() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                packagesListView.setSelection(0);
                packagesListView.performItemClick(packagesListView.getSelectedView(),
                        0, packagesListView.getItemIdAtPosition(0));
                editPkgBtn.callOnClick();
            }
        });

        lock.await(5000, TimeUnit.MILLISECONDS);

        Packages selectedPackage = null;
        try{
            selectedPackage = ((Packages)packagesListView.getSelectedItem()).fetchIfNeeded();
        }catch (ParseException e){}
        assertTrue(packageName.getText().toString().equals(selectedPackage.getName()));

        final double originalPrice = Double.parseDouble(price.getText().toString());
        final int originalDuration = Integer.parseInt(duration.getText().toString());

        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                price.setText(Double.toString(originalPrice + 1.5));
                duration.setText(Integer.toString(originalDuration + 5));
                commitChangesBtn.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);


        try {
            selectedPackage.fetch();
        }catch (ParseException e){}

        assertEquals(selectedPackage.getCost(), originalPrice+1.5);
        assertEquals(selectedPackage.getDuration(), originalDuration+5);

        selectedPackage.setCost(originalPrice);
        selectedPackage.setDuration(originalDuration);
        selectedPackage.saveInBackground();
        lock.await(1500, TimeUnit.MILLISECONDS);

        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                serviceListView.setSelection(0);
                serviceListView.performItemClick(serviceListView.getSelectedView(),
                        0, serviceListView.getItemIdAtPosition(0));
                commitChangesBtn.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
        ParseObject selectedService = null;
        try{
            selectedService = ((ParseObject)serviceListView.getSelectedItem()).fetchIfNeeded();
        }catch (ParseException e){}


        lock.await(5000, TimeUnit.MILLISECONDS);

        try {
            selectedPackage.fetch();
        }catch (ParseException e){}

        List<ParseObject> A = ((List<ParseObject>)selectedPackage.get("Services"));
        boolean flag = false;
        for(ParseObject o: A){
            String B = null;
            try {
                B = ((Service) o.fetch()).getName();
            }catch (ParseException e){}
            if (B.equals(((Service)selectedService).getName())) flag = true;
        }
        assertTrue(flag);
    }
}