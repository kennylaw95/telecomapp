package com.TeamGO.CSE110.test;

import android.test.ActivityInstrumentationTestCase2;
import com.TeamGO.CSE110.MyBillActivity;
import com.TeamGO.CSE110.Packages;
import com.TeamGO.CSE110.Services;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by flyfire2002 on 03/01/2015.
 */
public class MyBillActivityTest extends
        ActivityInstrumentationTestCase2<MyBillActivity> {
    MyBillActivity currActivity;

    ParseUser testUser;
    Services serviceStat, customerServices;

    public MyBillActivityTest(){
        super(MyBillActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        Parse.initialize(getInstrumentation().getTargetContext().getApplicationContext(),
                "AMTvK2ZetdSyAHBIiVnZaf4UVdOcLAfCTGsZdnke",
                "yFBpW6GF61xbO8N9PT3qTbIDLuESuyzSXMGx2LHP");
        try{
            ParseUser.logIn("flyfire2002", "12345678");
            testUser = ParseUser.getCurrentUser();
        }catch (ParseException e){
            throw e;
        }

        currActivity = getActivity();
    }

    public void testADisplay(){
        ParseQuery<Services> query = new ParseQuery<Services>("ServicesStatus");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<Services> objs = query.find();
            for (Services o : objs) {
                if (o.get("Username").equals(testUser.getUsername())) {
                    serviceStat = o;
                    customerServices = ParseObject.createWithoutData(Services.class, serviceStat.getObjectId());
                    break;
                }
            }

            customerServices.fetchIfNeeded();
        }catch (ParseException e){}

        List<ParseObject> srvcList = ((List<ParseObject>)customerServices.get("ServicesList"));
        double sum = 0;
        for(ParseObject o: srvcList){
            try {
                sum += o.fetchIfNeeded().getDouble("Price");
            }catch (ParseException e){}
        }
        List<Packages> pkgList = ((List<Packages>)customerServices.get("PackagesList"));
        for(Packages o: pkgList){
            try {
                o.fetchIfNeeded();
                sum += o.getCost();
            }catch (ParseException e){}
        }

        assertTrue(sum == customerServices.getDouble("Bill"));
    }
}