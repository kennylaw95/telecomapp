package com.TeamGO.CSE110.test;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

import com.TeamGO.CSE110.*;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by flyfire2002 on 03/03/2015.
 */
public class MarketingAddServiceTest extends
        ActivityInstrumentationTestCase2<MarketingAddService> {

    CountDownLatch lock = new CountDownLatch(1);
    MarketingAddService currActivity;

    Button internetButton;
    Button mobileButton;
    Button homeButton;

    //for EditText values
    EditText internetName;
    EditText internetPrice;
    EditText internetDuration;

    EditText mobileName;
    EditText mobilePrice;
    EditText mobileDuration;

    EditText homeName;
    EditText homePrice;
    EditText homeDuration;

    public MarketingAddServiceTest(){
        super(MarketingAddService.class);
    }

    protected void setUp() throws Exception {
        Parse.initialize(getInstrumentation().getTargetContext().getApplicationContext(),
                "AMTvK2ZetdSyAHBIiVnZaf4UVdOcLAfCTGsZdnke",
                "yFBpW6GF61xbO8N9PT3qTbIDLuESuyzSXMGx2LHP");
        try {
            ParseUser.logIn("flyfire2002", "12345678");
        } catch (ParseException e) {
            throw e;
        }

        currActivity = getActivity();

        internetName = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.iNameField);
        internetPrice = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.iPriceField);
        internetDuration = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.iDurationField);

        mobileName = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.mNameField);
        mobilePrice = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.mPriceField);
        mobileDuration = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.mDurationField);

        homeName = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.hNameField);
        homePrice = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.hPriceField);
        homeDuration = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.hDurationField);

        //Locate Buttons in XML
        internetButton = (Button) currActivity.findViewById(com.TeamGO.CSE110.R.id.internetButton);
        mobileButton = (Button) currActivity.findViewById(com.TeamGO.CSE110.R.id.mobileButton);
        homeButton = (Button) currActivity.findViewById(com.TeamGO.CSE110.R.id.homeButton);
    }

    public void testAAInternetNoInfo() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                internetName.setText("Test_NonExistent");
                internetButton.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
    }

    public void testABMobileNoInfo() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                mobilePrice.setText("157");
                mobileButton.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
    }

    public void testACHomeNoInfo() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                mobileDuration.setText("7");
                homeButton.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
    }

    public void testBAInternetAdd() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                Calendar cal = Calendar.getInstance();
                String currTime = cal.getTime().toString();
                internetName.setText("TestService"+currTime);
                internetPrice.setText("12");
                internetDuration.setText("7");
                internetButton.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);

        InternetService A = null;
        ParseQuery<InternetService> internetQuery = new ParseQuery<InternetService>("Internet");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<InternetService> objs = internetQuery.find();
            for (InternetService o : objs) {
                if (o.getName().contains("TestService")){
                    A = o;
                    break;
                }
            }
        }catch (ParseException e){}

        assertNotNull(A);
    }

    public void testBBMobileAdd() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                Calendar cal = Calendar.getInstance();
                String currTime = cal.getTime().toString();
                mobileName.setText("TestService"+currTime);
                mobilePrice.setText("12");
                mobileDuration.setText("7");
                mobileButton.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);

        WirelessService A = null;
        ParseQuery<WirelessService> wirelessQuery = new ParseQuery<WirelessService>("Wireless");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelessService> objs = wirelessQuery.find();
            for (WirelessService o : objs) {
                if (o.getName().contains("TestService")){
                    A = o;
                    break;
                }
            }
        }catch (ParseException e){}

        assertNotNull(A);
    }

    public void testBCHomeAdd() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                Calendar cal = Calendar.getInstance();
                String currTime = cal.getTime().toString();
                homeName.setText("TestService"+currTime);
                homePrice.setText("12");
                homeDuration.setText("7");
                homeButton.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);

        WirelineService A = null;
        ParseQuery<WirelineService> wirelineQuery = new ParseQuery<WirelineService>("Wireline");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelineService> objs = wirelineQuery.find();
            for (WirelineService o : objs) {
                if (o.getName().contains("TestService")){
                    A = o;
                    break;
                }
            }
        }catch (ParseException e){}

        assertNotNull(A);
    }
}