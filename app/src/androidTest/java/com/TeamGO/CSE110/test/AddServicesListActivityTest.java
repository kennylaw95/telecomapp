package com.TeamGO.CSE110.test;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.ListView;

import com.TeamGO.CSE110.AddServicesListActivity;
import com.TeamGO.CSE110.R;
import com.TeamGO.CSE110.Services;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class AddServicesListActivityTest extends
        ActivityInstrumentationTestCase2<AddServicesListActivity> {

    private CountDownLatch lock = new CountDownLatch(1);
    private AddServicesListActivity currActivity;
    Button addBtn;
    ParseUser testUser;
    Services serviceStat, customerServices;
    ListView serviceListView;

    public AddServicesListActivityTest() throws InterruptedException {
        super(AddServicesListActivity.class);

    }

    protected void setUp() throws Exception {
        super.setUp();
        Parse.initialize(getInstrumentation().getTargetContext().getApplicationContext(),
                "AMTvK2ZetdSyAHBIiVnZaf4UVdOcLAfCTGsZdnke",
                "yFBpW6GF61xbO8N9PT3qTbIDLuESuyzSXMGx2LHP");
        try {
            ParseUser.logIn("flyfire2002", "12345678");
            testUser = ParseUser.getCurrentUser();
        } catch (ParseException e) {
            throw e;
        }

        currActivity = getActivity(); //Get the activity under test

        ParseQuery<Services> query = new ParseQuery<Services>("ServicesStatus");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<Services> objs = query.find();
            for (Services o : objs) {
                if (o.get("Username").equals(testUser.getUsername())) {
                    serviceStat = o;
                    customerServices = ParseObject.createWithoutData(Services.class, serviceStat.getObjectId());
                    break;
                }
            }
        } catch (ParseException e) {
        }

        serviceListView = (ListView) currActivity.findViewById(R.id.servicelistview);
        addBtn = (Button) currActivity.findViewById(R.id.servicelistbutton);

        customerServices.init();
        try {
            customerServices.save();
        } catch (ParseException e) {
        }
    }

    public void testAAddNone() throws InterruptedException {
        addBtn.callOnClick();
        try {
            customerServices.fetchIfNeeded();
        } catch (ParseException e) {
        }
        assertTrue(((List<Services>) customerServices.get("ServicesList")).isEmpty());
    }

    public void testBAddNew() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                serviceListView.setSelection(1);
                serviceListView.performItemClick(serviceListView.getSelectedView(),
                        1, serviceListView.getItemIdAtPosition(1));
                addBtn.callOnClick();
            }
        });

        lock.await(1500, TimeUnit.MILLISECONDS);

        //addBtn.callOnClick();
        try {
            customerServices.fetchIfNeeded();
        } catch (ParseException e) {
        }
        assertFalse(((List<Services>) customerServices.get("ServicesList")).isEmpty());
    }

    public void testCAddDuplicate() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                serviceListView.setSelection(1);
                serviceListView.performItemClick(serviceListView.getSelectedView(),
                        1, serviceListView.getItemIdAtPosition(1));
            }
        });

        lock.await(1500, TimeUnit.MILLISECONDS);

        addBtn.callOnClick();
        try {
            customerServices.fetchIfNeeded();
        } catch (ParseException e) {
        }
        List<Services> A = ((List<Services>) customerServices.get("ServicesList"));
        addBtn.callOnClick();
        try {
            customerServices.fetchIfNeeded();
        } catch (ParseException e) {
        }
        List<Services> B = ((List<Services>) customerServices.get("ServicesList"));

        assertTrue(A.equals(B));
    }
}