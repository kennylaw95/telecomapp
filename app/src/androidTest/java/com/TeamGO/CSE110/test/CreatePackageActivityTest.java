package com.TeamGO.CSE110.test;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.TeamGO.CSE110.*;
import com.TeamGO.CSE110.R;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 * Created by flyfire2002 on 03/02/2015.
 */
public class CreatePackageActivityTest extends
        ActivityInstrumentationTestCase2<CreatePackageActivity> {
    Button createPkgBtn;
    CountDownLatch lock = new CountDownLatch(1);

    EditText packageName, duration, price;

    ListView serviceListView;

    CreatePackageActivity currActivity;

    public CreatePackageActivityTest(){
        super(CreatePackageActivity.class);
    }

    protected void setUp() throws Exception{
        Parse.initialize(getInstrumentation().getTargetContext().getApplicationContext(),
                "AMTvK2ZetdSyAHBIiVnZaf4UVdOcLAfCTGsZdnke",
                "yFBpW6GF61xbO8N9PT3qTbIDLuESuyzSXMGx2LHP");
        try {
            ParseUser.logIn("flyfire2002", "12345678");
        } catch (ParseException e) {
            throw e;
        }

        currActivity = getActivity();

        serviceListView = (ListView) currActivity.findViewById(com.TeamGO.CSE110.R.id.serviceList);
        packageName = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.packageName);
        price = (EditText) currActivity.findViewById(com.TeamGO.CSE110.R.id.price);
        duration = (EditText)currActivity.findViewById(com.TeamGO.CSE110.R.id.duration);
        createPkgBtn = (Button) currActivity.findViewById(R.id.createPackageBtn);
    }

    public void testAAPartialInfo()throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                packageName.setText("A");
                createPkgBtn.callOnClick();
            }
        });
        lock.await(2000, TimeUnit.MILLISECONDS);
    }
    public void testABPartialInfo()throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                price.setText("12");
                createPkgBtn.callOnClick();
            }
        });
        lock.await(2000, TimeUnit.MILLISECONDS);
    }
    public void testACPartialInfo()throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                packageName.setText("");
                duration.setText("46");
                createPkgBtn.callOnClick();
            }
        });
        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testBCreate()throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                serviceListView.setSelection(0);
                serviceListView.performItemClick(serviceListView.getSelectedView(),
                        0, serviceListView.getItemIdAtPosition(0));
                Calendar cal = Calendar.getInstance();
                String currTime = cal.getTime().toString();
                packageName.setText("TestPackage"+currTime);
                price.setText("12");
                duration.setText("46");
                createPkgBtn.callOnClick();
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
        ParseObject selectedService = null;
        try{
            selectedService = ((ParseObject)serviceListView.getSelectedItem()).fetchIfNeeded();
        }catch (ParseException e){}
        String pkgName = packageName.getText().toString();
        boolean flag = false;
        Packages pkg = null;
        ParseQuery<Packages> internetQuery = new ParseQuery<Packages>("Packages");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<Packages> objs = internetQuery.find();
            for (Packages o : objs) {
                o.fetch();
                if (o.getName().equals(pkgName)){
                    flag = true;
                    pkg = o;
                }
            }
        }catch (ParseException e){}

        assertTrue(flag);
        assertNotNull(pkg);

        if ((pkg.getCost() != 12) || (pkg.getDuration() != 46)) flag = false;
        assertTrue(flag);

        List<ParseObject> A = ((List<ParseObject>)pkg.get("Services"));
        for(ParseObject o: A){
            String B = null;
            try {
                B = ((Service) o.fetch()).getName();
            }catch (ParseException e){}
            if (B.equals(((Service)selectedService).getName())) flag = true;
        }
        assertTrue(flag);
    }
}
