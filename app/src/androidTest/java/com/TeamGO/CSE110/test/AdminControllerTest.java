package com.TeamGO.CSE110.test;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.TeamGO.CSE110.AddServicesListActivity;
import com.TeamGO.CSE110.AdminController;
import com.TeamGO.CSE110.Services;
import com.TeamGO.CSE110.R;
import com.TeamGO.CSE110.WirelineService;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by flyfire2002 on 03/01/2015.
 */
public class AdminControllerTest extends
        ActivityInstrumentationTestCase2<AdminController> {

    private CountDownLatch lock = new CountDownLatch(1);
    AdminController currActivity;
    Button addBtn, deleteBtn, searchUser;
    ParseUser testUser;
    Services serviceStat, customerServices;
    ListView subServicesListView, availServicesListView;
    EditText editTextUser;

    public AdminControllerTest() throws InterruptedException {
        super(AdminController.class);

    }

    protected void setUp() throws Exception {
        Parse.initialize(getInstrumentation().getTargetContext().getApplicationContext(),
                "AMTvK2ZetdSyAHBIiVnZaf4UVdOcLAfCTGsZdnke",
                "yFBpW6GF61xbO8N9PT3qTbIDLuESuyzSXMGx2LHP");
        try {
            ParseUser.logIn("testadmin1", "123");
            testUser = ParseUser.getCurrentUser();
        } catch (ParseException e) {
            throw e;
        }

        currActivity = getActivity();

        subServicesListView = (ListView)currActivity.findViewById(R.id.subsrvclistview);
        availServicesListView = (ListView)currActivity.findViewById(R.id.availsrvclistview);
        deleteBtn = (Button)currActivity.findViewById(R.id.admindeleteservicebutton);
        addBtn = (Button)currActivity.findViewById(R.id.adminaddservicebutton);

        editTextUser = (EditText)currActivity.findViewById(R.id.editTextUsers);
        searchUser = (Button)currActivity.findViewById(R.id.searchUserButton);
    }

    public void testAASearchForNone() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                editTextUser.setText("Test_NonExistent");
                searchUser.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
    }

    public void testABAAddForNoUser() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                addBtn.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
    }
    public void testABBDeleteForNoUser() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                deleteBtn.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
    }

    public void testACAAddForNone() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                editTextUser.setText("flyfire2002");
                searchUser.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                addBtn.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
    }

    public void testACBDeleteForNone() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                editTextUser.setText("flyfire2002");
                searchUser.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                deleteBtn.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);
    }

    public void testBAAdd() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                editTextUser.setText("flyfire2002");
                searchUser.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);


        ParseQuery<Services> query = new ParseQuery<Services>("ServicesStatus");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<Services> objs = query.find();
            for (Services o : objs) {
                if (o.get("Username").equals("flyfire2002")) {
                    serviceStat = o;
                    customerServices = ParseObject.createWithoutData(Services.class, serviceStat.getObjectId());
                    break;
                }
            }
        } catch (ParseException e) {}

        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                availServicesListView.setSelection(0);
                availServicesListView.performItemClick(availServicesListView.getSelectedView(),
                        0, availServicesListView.getItemIdAtPosition(0));
                addBtn.callOnClick();
            }
        });
    }

    public void testBBDelete() throws InterruptedException {
        ParseQuery<Services> query = new ParseQuery<Services>("ServicesStatus");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<Services> objs = query.find();
            for (Services o : objs) {
                if (o.get("Username").equals("flyfire2002")) {
                    serviceStat = o;
                    customerServices = ParseObject.createWithoutData(Services.class, serviceStat.getObjectId());
                    break;
                }
            }
        } catch (ParseException e) {
        }

        WirelineService A = null;
        ParseQuery<WirelineService> wirelineQuery = new ParseQuery<WirelineService>("Wireline");
        try {   //Use a not-background find to make user the user data is load when UI shows up
            List<WirelineService> objs = wirelineQuery.find();
            A = objs.get(0);
        }catch (ParseException e){}

        customerServices.addService(A);
        try {
            customerServices.save();
        }catch (ParseException e){}
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                editTextUser.setText("flyfire2002");
                searchUser.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);

        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                subServicesListView.setSelection(0);
                subServicesListView.performItemClick(subServicesListView.getSelectedView(),
                        0, subServicesListView.getItemIdAtPosition(0));
                deleteBtn.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
    }

    public void testBCAddAgain() throws InterruptedException {
        testBAAdd();
    }

    public void testBDAddDuplicate() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                editTextUser.setText("flyfire2002");
                searchUser.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);


        ParseQuery<Services> query = new ParseQuery<Services>("ServicesStatus");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<Services> objs = query.find();
            for (Services o : objs) {
                if (o.get("Username").equals("flyfire2002")) {
                    serviceStat = o;
                    customerServices = ParseObject.createWithoutData(Services.class, serviceStat.getObjectId());
                    break;
                }
            }
        } catch (ParseException e) {
        }
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                availServicesListView.setSelection(0);
                availServicesListView.performItemClick(availServicesListView.getSelectedView(),
                        0, availServicesListView.getItemIdAtPosition(0));
                addBtn.callOnClick();
            }
        });

        lock.await(5000, TimeUnit.MILLISECONDS);
    }

    public void testBEAddPackage() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                editTextUser.setText("flyfire2002");
                searchUser.callOnClick();
            }
        });
        lock.await(1500, TimeUnit.MILLISECONDS);


        ParseQuery<Services> query = new ParseQuery<Services>("ServicesStatus");
        try { //Use a not-background find to make user the user data is load when UI shows up
            List<Services> objs = query.find();
            for (Services o : objs) {
                if (o.get("Username").equals("flyfire2002")) {
                    serviceStat = o;
                    customerServices = ParseObject.createWithoutData(Services.class, serviceStat.getObjectId());
                    break;
                }
            }
        } catch (ParseException e) {
        }
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                availServicesListView.setSelection(availServicesListView.getCount()-1);
                availServicesListView.performItemClick(availServicesListView.getSelectedView(),
                        availServicesListView.getCount()-1,
                        availServicesListView.getItemIdAtPosition(availServicesListView.getCount()-1));
                addBtn.callOnClick();
            }
        });

        lock.await(5000, TimeUnit.MILLISECONDS);
    }
}
