package com.TeamGO.CSE110.test;

import java.util.Calendar;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

import com.TeamGO.CSE110.RetailSignUpActivity;
import com.TeamGO.CSE110.R;
import com.parse.ParseUser;

public class RetailSignUpActivityTest extends
        ActivityInstrumentationTestCase2<RetailSignUpActivity> {

    //To be used to make main thread wait for UI thread
    private CountDownLatch lock = new CountDownLatch(1);

    private RetailSignUpActivity currActivity;
    Button signup;
    EditText name;
    EditText address;
    EditText password;
    EditText passwordConfirm;
    EditText username;
    EditText cellphone;

    public RetailSignUpActivityTest() {
        super(RetailSignUpActivity.class);
    }

    //Will be called before every method
    protected void setUp() throws Exception {
        super.setUp();
        currActivity = getActivity(); //Get the activity under test
        //Get each element in the UI
        name = (EditText) currActivity.findViewById(R.id.name);
        username = (EditText) currActivity.findViewById(R.id.username);
        password = (EditText) currActivity.findViewById(R.id.password);
        passwordConfirm = (EditText) currActivity.findViewById(R.id.passwordConfirm);
        address = (EditText) currActivity.findViewById(R.id.address);
        signup = (Button) currActivity.findViewById(R.id.signup);
        cellphone = (EditText) currActivity.findViewById(R.id.phone);
    }

    //Test case 1: Non-Existent user
    public void testANoNameUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("");
                username.setText("test_NoNameUser");
                password.setText("12345678");
                passwordConfirm.setText("12345678");
                address.setText("UCSD GEISEL");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!RetailSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    //Test case 1: Non-Existent user
    public void testBNoUsernameUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Hattori Hanzo");
                username.setText("");
                password.setText("12345678");
                passwordConfirm.setText("12345678");
                address.setText("Okinawa");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!RetailSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    //Test case 1: Non-Existent user
    public void testCANoPasswordUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Akuma Stocking");
                username.setText("hellsing");
                password.setText("");
                passwordConfirm.setText("");
                address.setText("London, United Kingdom");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!RetailSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    public void testCBNoPasswordWithConfirmUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Akuma Stocking");
                username.setText("hellsing");
                password.setText("");
                passwordConfirm.setText("12345678");
                address.setText("London, United Kingdom");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!RetailSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    public void testCCNoPasswordConfirmUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Akuma Stocking");
                username.setText("hellsing");
                password.setText("12345678");
                passwordConfirm.setText("");
                address.setText("London, United Kingdom");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!RetailSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    public void testCDConfirmMismatchUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Akuma Stocking");
                username.setText("hellsing");
                password.setText("12345678");
                passwordConfirm.setText("87654321");
                address.setText("London, United Kingdom");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!RetailSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    //Test case 1: Non-Existent user
    public void testDNoAddressUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Romad Bill");
                username.setText("I_dont_have_add");
                password.setText("12345678");
                address.setText("");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!RetailSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    public void testDANoPhoneUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Romad Bill");
                username.setText("I_dont_have_add");
                password.setText("12345678");
                address.setText("3445");
                cellphone.setText("");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!RetailSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    //Test case 1: Non-Existent user
    public void testEExistentUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Ajax");
                username.setText("flyfire2002");
                password.setText("12345678");
                passwordConfirm.setText("12345678");
                address.setText("UCSD GEISEL");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!RetailSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    //Test case 1: Non-Existent user
    public void testZAvailUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                Calendar cal = Calendar.getInstance();
                String currTime = cal.getTime().toString();
                name.setText("SignUpTestUser"+currTime);
                username.setText("SignUpTestUser"+currTime);
                password.setText("12345678");
                passwordConfirm.setText("12345678");
                address.setText("UCSD GEISEL");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(RetailSignUpActivity.isSignupSuccess());
        ParseUser.logOut();
        //Assert if login failed
    }
}