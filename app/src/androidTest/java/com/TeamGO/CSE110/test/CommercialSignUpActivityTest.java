package com.TeamGO.CSE110.test;

import java.util.Calendar;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

import com.TeamGO.CSE110.CommercialSignUpActivity;
import com.TeamGO.CSE110.R;
import com.parse.ParseUser;

public class CommercialSignUpActivityTest extends
        ActivityInstrumentationTestCase2<CommercialSignUpActivity> {

    //To be used to make main thread wait for UI thread
    private CountDownLatch lock = new CountDownLatch(1);

    private CommercialSignUpActivity currActivity;
    Button signup;
    EditText name;
    EditText address;
    EditText password;
    EditText passwordConfirm;
    EditText businessName;
    EditText username;
    EditText cellphone;

    public CommercialSignUpActivityTest() {
        super(CommercialSignUpActivity.class);
    }

    //Will be called before every method
    protected void setUp() throws Exception {
        super.setUp();
        currActivity = getActivity(); //Get the activity under test
        //Get each element in the UI
        name = (EditText) currActivity.findViewById(R.id.personResponsible);
        username = (EditText) currActivity.findViewById(R.id.username);
        password = (EditText) currActivity.findViewById(R.id.password);
        passwordConfirm = (EditText) currActivity.findViewById(R.id.passwordConfirm);
        address = (EditText) currActivity.findViewById(R.id.address);
        businessName = (EditText) currActivity.findViewById(R.id.companyname);
        cellphone = (EditText) currActivity.findViewById(R.id.phone);
        signup = (Button) currActivity.findViewById(R.id.signup);

    }

    //Test case 1: Non-Existent user
    public void testANoNameUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("");
                username.setText("test_NoNameUserComm");
                password.setText("12345678");
                passwordConfirm.setText("12345678");
                address.setText("UCSD GEISEL");
                businessName.setText("FOR PROFIT EDU ORG");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!CommercialSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    //Test case 1: Non-Existent user
    public void testBNoUsernameUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Hattori Hanzo");
                username.setText("");
                password.setText("12345678");
                passwordConfirm.setText("12345678");
                address.setText("Okinawa");
                businessName.setText("Sushi Boat");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!CommercialSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    //Test case 1: Non-Existent user
    public void testCANoPasswordUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Akuma Stocking");
                username.setText("hellsing");
                password.setText("");
                passwordConfirm.setText("");
                address.setText("London, United Kingdom");
                businessName.setText("HELLSING ORGANIZATION");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!CommercialSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    public void testCBNoPasswordWithConfirmUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Akuma Stocking");
                username.setText("hellsing");
                password.setText("");
                passwordConfirm.setText("12345678");
                address.setText("London, United Kingdom");
                businessName.setText("HELLSING ORGANIZATION");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!CommercialSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    public void testCCNoPasswordConfirmUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Akuma Stocking");
                username.setText("hellsing");
                password.setText("12345678");
                passwordConfirm.setText("");
                address.setText("London, United Kingdom");
                businessName.setText("HELLSING ORGANIZATION");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!CommercialSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    public void testCDConfirmMismatchUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Akuma Stocking");
                username.setText("hellsing");
                password.setText("12345678");
                passwordConfirm.setText("87654321");
                address.setText("London, United Kingdom");
                businessName.setText("HELLSING ORGANIZATION");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!CommercialSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    //Test case 1: Non-Existent user
    public void testDNoAddressUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Romad Bill");
                username.setText("I_dont_have_add");
                password.setText("12345678");
                address.setText("");
                businessName.setText("Romad Bill Transportation");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!CommercialSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    //Test case 1: Non-Existent user
    public void testEExistentUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Ajax");
                username.setText("flyfire2002");
                password.setText("12345678");
                passwordConfirm.setText("12345678");
                address.setText("UCSD GEISEL");
                businessName.setText("MATT");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!CommercialSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    public void testFNoBusinessName() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Bull");
                username.setText("Briefcase");
                password.setText("12345678");
                passwordConfirm.setText("12345678");
                address.setText("Kayman Island");
                businessName.setText("");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!CommercialSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    public void testGNoPhoneName() throws InterruptedException{
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                name.setText("Bull");
                username.setText("Briefcase");
                password.setText("12345678");
                passwordConfirm.setText("12345678");
                address.setText("Kayman Island");
                businessName.setText("KIA");
                cellphone.setText("");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(!CommercialSignUpActivity.isSignupSuccess());
        //Assert if login failed
    }

    //Test case 1: Non-Existent user
    public void testZAvailUser() throws InterruptedException {
        currActivity.runOnUiThread(new Runnable() {
            public void run() {
                Calendar cal = Calendar.getInstance();
                String currTime = cal.getTime().toString();
                name.setText("SignUpTestUser"+currTime);
                username.setText("SignUpTestUser"+currTime);
                password.setText("12345678");
                passwordConfirm.setText("12345678");
                address.setText("UCSD GEISEL");
                businessName.setText("TeamGo");
                cellphone.setText("6197093395");
                signup.callOnClick();
            }
        });
        lock.await(5000, TimeUnit.MILLISECONDS);
        //Wait 5 secs to allow Parse server return verification result.
        assertTrue(CommercialSignUpActivity.isSignupSuccess());
        ParseUser.logOut();
        //Assert if login failed
    }
}