ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* ic_launcher-web.png
* proguard-project.txt
* teamGoTest-test/
* teamGoTest-test/.classpath
* teamGoTest-test/.project
* teamGoTest-test/.settings/
* teamGoTest-test/.settings/org.eclipse.jdt.core.prefs
* teamGoTest-test/bin/
* teamGoTest-test/bin/AndroidManifest.xml
* teamGoTest-test/bin/classes.dex
* teamGoTest-test/bin/res/
* teamGoTest-test/bin/res/crunch/
* teamGoTest-test/bin/res/crunch/drawable-hdpi/
* teamGoTest-test/bin/res/crunch/drawable-hdpi/ic_launcher.png
* teamGoTest-test/bin/res/crunch/drawable-ldpi/
* teamGoTest-test/bin/res/crunch/drawable-ldpi/ic_launcher.png
* teamGoTest-test/bin/res/crunch/drawable-mdpi/
* teamGoTest-test/bin/res/crunch/drawable-mdpi/ic_launcher.png
* teamGoTest-test/bin/res/crunch/drawable-xhdpi/
* teamGoTest-test/bin/res/crunch/drawable-xhdpi/ic_launcher.png
* teamGoTest-test/bin/resources.ap_
* teamGoTest-test/bin/teamGoTest.apk
* teamGoTest-test/proguard-project.txt
* teamGoTest-test/project.properties

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:21.0.3

Replaced Libraries with Dependencies:
-------------------------------------
The importer recognized the following library projects as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the source files in your project were of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the library replacement in the import wizard and try
again:

appcompat-v7 => [com.android.support:appcompat-v7:21.0.3]

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app/src/main/AndroidManifest.xml
* libs/Parse-1.8.2.jar => app/libs/Parse-1.8.2.jar
* libs/ParseCrashReporting-1.8.2.jar => app/libs/ParseCrashReporting-1.8.2.jar
* libs/bolts-android-1.1.4-javadoc.jar => app/libs/bolts-android-1.1.4-javadoc.jar
* libs/bolts-android-1.1.4.jar => app/libs/bolts-android-1.1.4.jar
* res/ => app/src/main/res/
* src/ => app/src/main/java/
* teamGoTest-test/res/ => app/src/androidTest/res/
* teamGoTest-test/src/ => app/src/androidTest/java/

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
